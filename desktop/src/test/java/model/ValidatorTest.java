package model;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class ValidatorTest {

    @Test
    void validatorTest() {
        String sValue=null;
        Double dValue=null;
        assertFalse(Validator.isValid(sValue));
        assertFalse(Validator.isValid(new String()));
        assertTrue(Validator.isValid("A"));
        assertTrue(Validator.isValid("Validator test"));
        ////
        assertFalse(Validator.isValid(dValue));
        assertFalse(Validator.isValid(-122.3));
        assertFalse(Validator.isValid(-0.000000001));
        assertTrue(Validator.isValid(0.0));
        assertTrue(Validator.isValid(0.000000000000001));
        assertTrue(Validator.isValid(123.4));
        ////
        assertFalse(Validator.isValid(dValue,null,null));
        assertFalse(Validator.isValid(dValue,10.2,21.3));
        assertFalse(Validator.isValid(100.0,null,null));
        assertFalse(Validator.isValid(100.0,100.00001,null));
        assertFalse(Validator.isValid(100.0,null,99.999999999));
        assertTrue(Validator.isValid(100.0,99.999999,null));
        assertTrue(Validator.isValid(100.0,null,100.0000001));
        assertFalse(Validator.isValid(100.0,100.00001,100.00003));
        assertFalse(Validator.isValid(100.5,100.00001,100.00003));
        assertTrue(Validator.isValid(100.00002,100.00001,100.00003));

    }
    @Test
    void validatorUserTest() {
        User user = new User();
        user.setId(102);
        user.setType(User.TYPE_PLANT_PROTECTION);
        user.setFirstName("Ashley");
        user.setLastName("Williams");
        user.setObtainingDate(LocalDate.now());
        assertTrue(Validator.hasValidCertificate(user));
        user.setObtainingDate(LocalDate.now().minusYears(1).minusDays(10));
        assertTrue(Validator.hasValidCertificate(user));
        user.setObtainingDate(LocalDate.now().minusYears(5).plusDays(10));
        assertTrue(Validator.hasValidCertificate(user));
        user.setObtainingDate(LocalDate.now().minusYears(5).plusDays(1));
        assertTrue(Validator.hasValidCertificate(user));
        user.setObtainingDate(LocalDate.now().minusYears(5));
        assertFalse(Validator.hasValidCertificate(user));
        user.setObtainingDate(LocalDate.now().minusYears(5).minusDays(1));
        assertFalse(Validator.hasValidCertificate(user));
        user.setObtainingDate(LocalDate.now().minusYears(5).minusDays(10));
        assertFalse(Validator.hasValidCertificate(user));
    }
}