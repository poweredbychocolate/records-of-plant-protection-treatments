package services;

import model.PlantProtectionProduct;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RecordsFileLoadTest {
    private RecordFileService service = new RecordFileService();
    private DatabaseService databaseService = new DatabaseService();

    @Test
    void readPlantProtectionProducts() {
        assertNotNull(service);
        assertNotNull(databaseService);
        List<PlantProtectionProduct> localList = null;
        //try read not existing file
        try {
            localList = service.readPlantProtectionProducts(new FileInputStream(new File("test")));
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
        }
        assertNull(localList);
        //try read file from resources
        try {
            //TODO edit if sor file was edited
            localList = service.readPlantProtectionProducts(new FileInputStream(new File("src/test/resources/sor.xls")));
            assertNotNull(localList);
            assertTrue(databaseService.save(localList));
            assertEquals(2264, localList.size());
            assertEquals(27, databaseService.findProductTypes().size());
            assertEquals("Akarycyd", databaseService.findProductTypes().get(1));
            assertNotEquals("Akarycyd", databaseService.findProductTypes().get(5));
            String type = "Akarycyd";
            List<PlantProtectionProduct> productList = databaseService.findAllProduct(null, type);
            assertTrue(productList.get(0).getType().contains(type));
            assertTrue(productList.get(2).getType().contains(type));
            assertTrue(productList.get(5).getType().contains(type));
            assertTrue(productList.get(10).getType().contains(type));
            assertTrue(productList.get(12).getType().contains(type));
            productList = databaseService.findAllProduct("as", null);
            assertTrue(productList.size() > 0);
            assertEquals(productList.size(), databaseService.findAllProduct("AS", null).size());
            assertEquals(productList.size(), databaseService.findAllProduct("As", null).size());
            assertEquals(productList.size(), databaseService.findAllProduct("aS", null).size());
            assertTrue(databaseService.findAllProduct("As", null).size() > databaseService.findAllProduct("As", "Fungicyd").size());
            System.err.println(databaseService.findProductNames("H"));
            assertNotNull(databaseService.findProductNames("A"));
            assertEquals(197,databaseService.findProductNames("A").size());
            assertNotNull(databaseService.findProductNames("Abringo"));
            assertEquals(1,databaseService.findProductNames("Abringo").size());
            assertNotNull(databaseService.findProductNames("He"));
            assertEquals(25,databaseService.findProductNames("He").size());
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
        }
    }
}