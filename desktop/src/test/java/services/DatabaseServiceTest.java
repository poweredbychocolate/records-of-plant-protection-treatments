package services;

import model.PlantProtectionRecord;
import model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Database service test.
 *
 * @author Dawid
 * @version 2
 */
class DatabaseServiceTest {

    private DatabaseService databaseService;
    private User user1, user2;
    private PlantProtectionRecord record1, record2, record3;


    @BeforeEach
    void BeforeEach() {
        databaseService = new DatabaseService();
        user1 = new User();
        user1.setFirstName("Liara");
        user1.setLastName("T'Soni");
        user1.setType(User.TYPE_PLANT_PROTECTION);
        user1.setObtainingDate(LocalDate.now());

        user2 = new User();
        user2.setFirstName("Tali");
        user2.setLastName("Zorah");
        user2.setType(User.TYPE_PLANT_PROTECTION);
        user2.setObtainingDate(LocalDate.now().minusYears(3));

        record1 = new PlantProtectionRecord();
        record1.setDate(LocalDate.now().minusDays(5));
        record1.setPlant("Rose of Citadel");
        record1.setTotalArea(100.0);
        record1.setTreatmentArea(83.2);
        record1.setAreaName("Eden Prime");
        record1.setProtectionName("Asari Touch");
        record1.setProtectionDose(1.25);
        record1.setLiquidDose(20.0);
        record1.setReasonUse("Reapers is here");
        record1.setComments("Because reapers attack us we try protect our human using asari touch");

        record2 = new PlantProtectionRecord();
        record2.setDate(LocalDate.now().minusDays(3));
        record2.setPlant("Electric Bloody Drop");
        record2.setTotalArea(560.0);
        record2.setTreatmentArea(362.8);
        record2.setAreaName("Eden Prime");
        record2.setProtectionName("Quarians Star Dust");
        record2.setProtectionDose(2.56);
        record2.setLiquidDose(204.2);
        record2.setReasonUse("Black leaf");
        record2.setComments("Black leaf,same leaves on ground, rainy,windy");

        record3 = new PlantProtectionRecord();
        record3.setDate(LocalDate.now());
        record3.setPlant("Orange trees");
        record3.setTotalArea(60.0);
        record3.setTreatmentArea(60.0);
        record3.setAreaName("Thessia");
        record3.setProtectionName("Black drop");
        record3.setProtectionDose(4.56);
        record3.setLiquidDose(364.2);
        record3.setReasonUse("Dropping leaves");
        record3.setComments("Dropping leaves,sunny,windy");
    }

    /**
     * Test users.
     */
    @Test
    void testUsers() {

        assertNotNull(databaseService);
        User lUser = databaseService.save(user1);
        assertNotNull(lUser);
        assertNotEquals(user1, lUser);
        assertEquals(user1.getFirstName(), lUser.getFirstName());
        assertEquals(user1.getLastName(), lUser.getLastName());
        assertEquals(lUser, databaseService.save(lUser));
        assertNotNull(databaseService.save(user2));

        assertEquals(2, databaseService.findAll(User.class).size());
        List<User> list;
        assertNotNull(list = databaseService.findAll(User.class));
        assertNotEquals(0, list);
        assertFalse(databaseService.remove(User.class, user2));
        for (User u : list) {
            assertTrue(databaseService.remove(User.class, u));
        }
        assertEquals(0, databaseService.findAll(User.class).size());
        User user10, user20;
        assertNotNull(user10 = databaseService.save(user1));
        assertNotNull(user20 = databaseService.save(user2));
        assertTrue(databaseService.remove(User.class, user20));
        assertTrue(databaseService.remove(User.class, user10));
        assertFalse(databaseService.remove(User.class, user2));
        assertFalse(databaseService.remove(User.class, user1));

        assertEquals(0, databaseService.findAll(User.class).size());
        assertNotNull(user10 = databaseService.save(user1));
        assertNotNull(user20 = databaseService.save(user2));
        assertNotEquals(0, databaseService.findAll(User.class).size());
        assertEquals(2, databaseService.findAll(User.class).size());
        assertNotNull(databaseService.findById(User.class, user10.getId()));
        assertNotNull(databaseService.findById(User.class, user20.getId()));
        assertEquals(user10, databaseService.findById(User.class, user10.getId()));
        assertEquals(user20, databaseService.findById(User.class, user20.getId()));
        assertNotEquals(user10, databaseService.findById(User.class, user20.getId()));
        assertNotEquals(user20, databaseService.findById(User.class, user10.getId()));
        assertNull(databaseService.findById(User.class, 1000));
    }

    @Test
    void testPlantProtectionRecord() {

        assertNotNull(databaseService);
        User localUser1 = databaseService.save(user1);
        User localUser2 = databaseService.save(user2);
        record1.setUser(localUser1);
        PlantProtectionRecord localRecord1 = databaseService.save(record1);

        assertNotNull(localRecord1);
        assertNotEquals(record1, localRecord1);
        assertEquals(record1.getDate(), localRecord1.getDate());
        assertEquals(record1.getPlant(), localRecord1.getPlant());
        assertEquals(record1.getTotalArea(), localRecord1.getTotalArea());
        assertEquals(record1.getTreatmentArea(), localRecord1.getTreatmentArea());
        assertEquals(record1.getAreaName(), localRecord1.getAreaName());
        assertEquals(record1.getProtectionName(), localRecord1.getProtectionName());
        assertEquals(record1.getProtectionDose(), localRecord1.getProtectionDose());
        assertEquals(record1.getLiquidDose(), localRecord1.getLiquidDose());
        assertEquals(record1.getReasonUse(), localRecord1.getReasonUse());
        assertEquals(record1.getComments(), localRecord1.getComments());
        assertEquals(record1.getUser(), localRecord1.getUser());
        assertNotEquals(record1, record2);
        assertNotEquals(record1, record3);
        assertNotEquals(record2, record3);

        record2.setUser(localUser1);
        record3.setUser(localUser2);
        PlantProtectionRecord localRecord2 = databaseService.save(record2);
        PlantProtectionRecord localRecord3 = databaseService.save(record3);
        assertNotNull(localRecord2);
        assertNotNull(localRecord3);
        assertNotEquals(localRecord2, localRecord3);
        assertEquals(3, databaseService.findAll(PlantProtectionRecord.class).size());
        ////
        List<PlantProtectionRecord> list;
        assertNotNull(list = databaseService.findAll(PlantProtectionRecord.class));
        assertNotEquals(0, list);
        assertFalse(databaseService.remove(PlantProtectionRecord.class, record2));
        assertEquals(3, databaseService.findAll(PlantProtectionRecord.class).size());
        for (PlantProtectionRecord o : list) {
            assertTrue(databaseService.remove(PlantProtectionRecord.class, o));
        }
        assertEquals(0, databaseService.findAll(PlantProtectionRecord.class).size());
        assertNotNull(localRecord1 = databaseService.save(record1));
        assertNotNull(localRecord2 = databaseService.save(record2));
        assertTrue(databaseService.remove(PlantProtectionRecord.class, localRecord1));
        assertTrue(databaseService.remove(PlantProtectionRecord.class, localRecord2));
        assertFalse(databaseService.remove(PlantProtectionRecord.class, record1));
        assertFalse(databaseService.remove(PlantProtectionRecord.class, record2));
        ////
        assertEquals(0, databaseService.findAll(PlantProtectionRecord.class).size());
        assertNotNull(localRecord1 = databaseService.save(record1));
        assertNotNull(localRecord2 = databaseService.save(record2));
        assertNotEquals(0, databaseService.findAll(PlantProtectionRecord.class).size());
        assertEquals(2, databaseService.findAll(PlantProtectionRecord.class).size());
        assertNotNull(databaseService.findById(PlantProtectionRecord.class, localRecord1.getId()));
        assertNotNull(databaseService.findById(PlantProtectionRecord.class, localRecord2.getId()));
        assertEquals(localRecord1, databaseService.findById(PlantProtectionRecord.class, localRecord1.getId()));
        assertEquals(localRecord2, databaseService.findById(PlantProtectionRecord.class, localRecord2.getId()));
        assertNotEquals(localRecord1, databaseService.findById(PlantProtectionRecord.class, localRecord2.getId()));
        assertNotEquals(localRecord2, databaseService.findById(PlantProtectionRecord.class, localRecord1.getId()));
        assertNull(databaseService.findById(PlantProtectionRecord.class, 1000));
    }

    @Test
    void findExpiringCertificates() {
        assertNotNull(databaseService.save(user1));
        assertNotNull(databaseService.save(user2));
        User u3 = new User();
        u3.setFirstName("Miranda");
        u3.setLastName("Lawson");
        u3.setType(User.TYPE_PLANT_PROTECTION);
        u3.setObtainingDate(LocalDate.now());
        User u4 = new User();
        u4.setFirstName("Test1");
        u4.setLastName("Test1");
        u4.setType(User.TYPE_PLANT_PROTECTION);
        u4.setObtainingDate(LocalDate.now().minusDays(1));
        User u5 = new User();
        u5.setFirstName("Test2");
        u5.setLastName("Test2");
        u5.setType(User.TYPE_PLANT_PROTECTION);
        u5.setObtainingDate(LocalDate.now().minusDays(2));
        User u6 = new User();
        u6.setFirstName("Test3");
        u6.setLastName("Test3");
        u6.setType(User.TYPE_PLANT_PROTECTION);
        u6.setObtainingDate(LocalDate.now().minusDays(3));
        User u7 = new User();
        u7.setFirstName("Test4");
        u7.setLastName("Test4");
        u7.setType(User.TYPE_PLANT_PROTECTION);
        u7.setObtainingDate(LocalDate.now().minusDays(4));
        User u8 = new User();
        u8.setFirstName("Test5");
        u8.setLastName("Test5");
        u8.setType(User.TYPE_PLANT_PROTECTION);
        u8.setObtainingDate(LocalDate.now().minusDays(5));

        assertNotNull(databaseService.save(u3));
        assertNotNull(databaseService.save(u4));
        assertNotNull(databaseService.save(u5));
        assertNotNull(databaseService.save(u6));
        assertNotNull(databaseService.save(u7));
        assertNotNull(databaseService.save(u8));

        assertEquals(8, databaseService.findAll(User.class).size());
        List<User> list = databaseService.findExpiringCertificates(LocalDate.now().minusDays(3));
        assertNotNull(list);
        assertEquals(4, list.size());
    }
}