package services;

import model.PlantProtectionProduct;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

import java.io.FileInputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class RecordFileService {

    public List<PlantProtectionProduct> readPlantProtectionProducts(FileInputStream inputStream) {
        try {
            List<PlantProtectionProduct> list = new ArrayList<>();
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            HSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.rowIterator();
            //skip header row
            if (rowIterator.hasNext()) rowIterator.next();
            int i = 1;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                //skip other column
                if (row.getPhysicalNumberOfCells() != 16) continue;
                Iterator<Cell> cellIterator = row.cellIterator();
                PlantProtectionProduct product = new PlantProtectionProduct();
                //skip id from file
                cellIterator.next();
                ////
                Cell cell = cellIterator.next();
                product.setAuthorizationNumber(cell.getStringCellValue());
                cell = cellIterator.next();
                product.setName(cell.getStringCellValue());
                cell = cellIterator.next();
                product.setType(cell.getStringCellValue());
                cell = cellIterator.next();
                product.setRegisteredOffice(cell.getStringCellValue());
                cell = cellIterator.next();
                product.setManufacturer(cell.getStringCellValue());
                cell = cellIterator.next();
                product.setActiveSubstance(cell.getStringCellValue());
                ////
                cell = cellIterator.next();
                if (cell.getCellType().equals(CellType.STRING)) {
                    product.setClassificationHumanHealth(cell.getStringCellValue());
                }
                cell = cellIterator.next();
                if (cell.getCellType().equals(CellType.STRING)) {
                    product.setClassificationBeesHazard(cell.getStringCellValue());
                }
                cell = cellIterator.next();
                if (cell.getCellType().equals(CellType.STRING)) {
                    product.setClassificationAquaticOrganisms(cell.getStringCellValue());
                }
                cell = cellIterator.next();
                if (cell.getCellType().equals(CellType.STRING)) {
                    product.setClassificationEnvironmentalHazards(cell.getStringCellValue());
                }
                ////
                cell = cellIterator.next();
                if (cell.getCellType().equals(CellType.NUMERIC)) {
                    product.setValidityTerm(convertDate(cell.getDateCellValue()));
                }
                cell = cellIterator.next();
                if (cell.getCellType().equals(CellType.NUMERIC)) {
                    product.setSalePeriod(convertDate(cell.getDateCellValue()));
                }
                cell = cellIterator.next();
                if (cell.getCellType().equals(CellType.NUMERIC)) {
                    product.setUsePeriod(convertDate(cell.getDateCellValue()));
                }
                cell = cellIterator.next();
                if (cell.getCellType().equals(CellType.STRING)) {
                    product.setPackagingType(cell.getStringCellValue());
                }
                ////
                list.add(product);
            }
            return list;
        } catch (Exception e) {
            System.err.println("Workbook not available");
            DiscordLogService.get().write("Protection Workbook Error", LocalDateTime.now(),e);
        }
        return null;
    }

    private LocalDate convertDate(Date date) {
        return date != null ? date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate() : null;
    }
}
