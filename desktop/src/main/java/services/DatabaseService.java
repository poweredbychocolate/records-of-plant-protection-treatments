package services;

import model.AgriculturalSale;
import model.EntityId;
import model.PlantProtectionProduct;
import model.User;
import org.hibernate.*;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Database service perform all data storage task
 *
 * @author Dawid
 * @version 6
 * @since 2019 -03-31
 */
public class DatabaseService implements AutoCloseable {
    private SessionFactory sessionFactory;
    private static DatabaseService databaseService;

    /**
     * Instantiates a new Database service.
     */
    public DatabaseService() {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
        Metadata metadata = new MetadataSources(registry).getMetadataBuilder().build();
        sessionFactory = metadata.getSessionFactoryBuilder().build();
    }

    /**
     * Gets {@link DatabaseService} instance.
     *
     * @return the {@link DatabaseService} object
     */
    public static DatabaseService getDB() {
        if (databaseService == null) databaseService = new DatabaseService();
        return databaseService;
    }

    @Override
    public void close() {
        sessionFactory.close();
    }


    /**
     * Save entity.
     *
     * @param <T>    entity type parameter
     * @param toSave entity to save
     * @return saved entity
     */
    public synchronized <T> T save(T toSave) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            T tmp = (T) session.merge(toSave);
            transaction.commit();
            return tmp;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            return null;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }


    /**
     * Remove entity.
     *
     * @param <T>      entity type parameter
     * @param type     entity class
     * @param toRemove entity to remove
     * @return operation status
     */
    public synchronized <T extends EntityId> boolean remove(Class<T> type, T toRemove) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            T tmp = session.find(type, toRemove.getId());
            if (tmp != null) {
                session.remove(tmp);
                transaction.commit();
                return true;
            }
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return false;
    }

    /**
     * Find all entries list.
     *
     * @param <T>  entity type parameter
     * @param type entity class
     * @return entries list
     */
    public <T> List<T> findAll(Class<T> type) {
        List<T> list = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<T> criteriaQuery = builder.createQuery(type);
            Root<T> root = criteriaQuery.from(type);
            criteriaQuery.select(root);
            list = session.createQuery(criteriaQuery).getResultList();

        } finally {
            if (session != null) {
                session.close();
            }
            return list;
        }
    }

    /**
     * Find entity by id.
     *
     * @param <T>  the type parameter
     * @param type the type
     * @param id   the id
     * @return the t
     */
    public <T> T findById(Class<T> type, Integer id) {
        T object = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            object = session.find(type, id);
        } finally {
            if (session != null) {
                session.close();
            }
            return object;
        }
    }

    /**
     * Find expiring certificates list.
     *
     * @param date the date
     * @return the list
     */
    public List<User> findExpiringCertificates(LocalDate date) {
        List<User> list = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<User> criteriaQuery = builder.createQuery(User.class);
            Root<User> root = criteriaQuery.from(User.class);
            criteriaQuery.select(root);
            criteriaQuery.where(builder.lessThanOrEqualTo(root.get("obtainingDate"), date));
            list = session.createQuery(criteriaQuery).getResultList();
        } finally {
            if (session != null) {
                session.close();
            }
            return list;
        }
    }

    /**
     * Save entries list.
     *
     * @param <T>    the type parameter
     * @param toSave the to save
     * @return operation status
     */
    public synchronized <T> boolean save(List<T> toSave) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            int counter = 0;
            for (T tmp : toSave) {
                session.merge(tmp);
                counter++;
                if (counter >= 100) {
                    counter = 0;
                    session.flush();
                    session.clear();
                }
            }
            transaction.commit();
            return true;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            return false;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * Find all product list.
     *
     * @return the list
     */
    public List<PlantProtectionProduct> findAllProduct() {
        List<PlantProtectionProduct> list = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<PlantProtectionProduct> criteriaQuery = builder.createQuery(PlantProtectionProduct.class);
            Root<PlantProtectionProduct> root = criteriaQuery.from(PlantProtectionProduct.class);
            criteriaQuery.select(root);
            criteriaQuery.orderBy(builder.asc(root.get("name")));
            list = session.createQuery(criteriaQuery).getResultList();
        } finally {
            if (session != null) {
                session.close();
            }
            return list;
        }
    }

    /**
     * Find product types list.
     *
     * @return the list
     */
    public List<String> findProductTypes() {
        List<String> list = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<String> criteriaQuery = builder.createQuery(String.class);
            Root<PlantProtectionProduct> root = criteriaQuery.from(PlantProtectionProduct.class);
            criteriaQuery.select(root.get("type")).distinct(true);
            criteriaQuery.orderBy(builder.asc(root.get("type")));
            list = session.createQuery(criteriaQuery).getResultList();
        } finally {
            if (session != null) {
                session.close();
            }
            return list;
        }
    }

    /**
     * Find all product list.
     *
     * @param name the name
     * @param type the type
     * @return the list
     */
    public List<PlantProtectionProduct> findAllProduct(String name, String type) {
        List<PlantProtectionProduct> list = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<PlantProtectionProduct> criteriaQuery = builder.createQuery(PlantProtectionProduct.class);
            Root<PlantProtectionProduct> root = criteriaQuery.from(PlantProtectionProduct.class);
            criteriaQuery.select(root);
            List<Predicate> predicates = new ArrayList<>();
            if (name != null && !name.equals("")) {
                Predicate predicate = builder.like(builder.lower(root.get("name")), name.toLowerCase() + "%");
                predicates.add(predicate);
            }
            if (type != null) {
                Predicate predicate = builder.like(root.get("type"), type.equals("") ? type : "%" + type + "%");
                predicates.add(predicate);
            }
            if (predicates.size() == 1) {
                criteriaQuery.where(predicates.get(0));
            }
            if (predicates.size() > 1) {
                criteriaQuery.where(builder.and(predicates.toArray(new Predicate[]{})));
            }

            criteriaQuery.orderBy(builder.asc(root.get("name")));
            list = session.createQuery(criteriaQuery).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
            return list;
        }
    }

    /**
     * Find product names list.
     *
     * @param pattern the pattern
     * @return the list
     */
    public List<String> findProductNames(String pattern) {
        List<String> list = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<String> criteriaQuery = builder.createQuery(String.class);
            Root<PlantProtectionProduct> root = criteriaQuery.from(PlantProtectionProduct.class);
            criteriaQuery.select(root.get("name"));
            criteriaQuery.orderBy(builder.asc(root.get("name")));
            criteriaQuery.where(builder.like(builder.lower(root.get("name")), pattern.toLowerCase() + "%"));
            list = session.createQuery(criteriaQuery).getResultList();
        } finally {
            if (session != null) {
                session.close();
            }
            return list;
        }
    }

    /**
     * Find sale names list.
     *
     * @return the list
     */
    public List<String> findSaleNames() {
        List<String> list = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<String> criteriaQuery = builder.createQuery(String.class);
            Root<AgriculturalSale> root = criteriaQuery.from(AgriculturalSale.class);
            criteriaQuery.select(root.get("name")).distinct(true);
            criteriaQuery.orderBy(builder.asc(root.get("name")));
            list = session.createQuery(criteriaQuery).getResultList();
        } finally {
            if (session != null) {
                session.close();
            }
            return list;
        }
    }

    /**
     * Find sale dates list.
     *
     * @return the list
     */
    public List<String> findSaleDates() {
        List<String> list = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<LocalDate> criteriaQuery = builder.createQuery(LocalDate.class);
            Root<AgriculturalSale> root = criteriaQuery.from(AgriculturalSale.class);
            criteriaQuery.select(root.get("date"));
            criteriaQuery.orderBy(builder.asc(root.get("date")));
            List<Integer> tmp=session.createQuery(criteriaQuery).getResultList().stream().map(LocalDate::getYear).distinct().collect(Collectors.toList());
            list = tmp.stream().map(Object::toString).collect(Collectors.toList());
        } finally {
            if (session != null) {
                session.close();
            }
            return list;
        }
    }

    /**
     * Find all sale list.
     *
     * @param date the date
     * @param name the name
     * @return the list
     */
    public List<AgriculturalSale> findAllSale(String date, String name) {
        List<AgriculturalSale> list = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<AgriculturalSale> criteriaQuery = builder.createQuery(AgriculturalSale.class);
            Root<AgriculturalSale> root = criteriaQuery.from(AgriculturalSale.class);
            criteriaQuery.select(root);
            List<Predicate> predicates = new ArrayList<>();
            if (date != null) {
//                Predicate predicate = builder.like(root.get("date"), "%"+ DateTimeFormatter.ofPattern("yyyy").parse(date) + "%");
                Predicate predicate = builder.and(builder.lessThanOrEqualTo(root.get("date"),LocalDate.of(Integer.parseInt(date), Month.DECEMBER,31)),
                        builder.greaterThanOrEqualTo(root.get("date"),LocalDate.of(Integer.parseInt(date), Month.JANUARY,1)));
                predicates.add(predicate);
            }
            if (name != null) {
                Predicate predicate = builder.like(root.get("name"), name + "%");
                predicates.add(predicate);
            }
            if (predicates.size() == 1) {
                criteriaQuery.where(predicates.get(0));
            }
            if (predicates.size() > 1) {
                criteriaQuery.where(builder.and(predicates.toArray(new Predicate[]{})));
            }
            criteriaQuery.orderBy(builder.asc(root.get("date")),builder.asc(root.get("name")));
            list = session.createQuery(criteriaQuery).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
            return list;
        }
    }

    /**
     * Find sales names list.
     *
     * @param pattern the pattern
     * @return the list
     */
    public List<String> findSalesNames(String pattern) {
        List<String> list = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<String> criteriaQuery = builder.createQuery(String.class);
            Root<AgriculturalSale> root = criteriaQuery.from(AgriculturalSale.class);
            criteriaQuery.select(root.get("name")).distinct(true);
            criteriaQuery.orderBy(builder.asc(root.get("name")));
            criteriaQuery.where(builder.like(builder.lower(root.get("name")), pattern.toLowerCase() + "%"));
            list = session.createQuery(criteriaQuery).getResultList();
        } finally {
            if (session != null) {
                session.close();
            }
            return list;
        }
    }
}
