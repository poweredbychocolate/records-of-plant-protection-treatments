package controllers;

import controllers.dialogs.SalesDialogController;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import model.AgriculturalSale;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import services.DatabaseService;
import services.DiscordLogService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * The type Sales controller.
 * @author Dawid
 * @version 1
 */
public class SalesController implements Initializable {
    private DatabaseService databaseService;
    private ResourceBundle resourceBundle;
    @FXML
    private TableView<AgriculturalSale> salesTable;
    @FXML
    private TableColumn<AgriculturalSale, String> totalPrice;
    @FXML
    private Button addSale;
    @FXML
    private Button excelSale;
    @FXML
    private ChoiceBox<String> sName;
    @FXML
    private ChoiceBox<String> sDate;
    private List<String> namesList;
    private List<String> datesList;
    private Pane saleDialogFXML;
    private Dialog<AgriculturalSale> saleDialog;
    private SalesDialogController dialogController;

    private ObservableList<AgriculturalSale> observableList() {
        ObservableList<AgriculturalSale> list = FXCollections.observableList(databaseService.findAllSale(null,null));
        return list;
    }

    private ObservableList<AgriculturalSale> observableList(String date, String name) {
        ObservableList<AgriculturalSale> list = FXCollections.observableList(databaseService.findAllSale(date, name));
        return list;
    }
    private void applyFilters(String d, String n){
        if(n!=null&&n.equals(resourceBundle.getString("label.all.sale.names"))) n=null;
        if(d!=null&&d.equals(resourceBundle.getString("label.all.sale.dates"))) d=null;
        salesTable.setItems(observableList(d,n));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resourceBundle = resources;
        this.databaseService = DatabaseService.getDB();
        Platform.runLater(() -> {
                    salesTable.setItems(observableList());
                    datesList = new ArrayList<>();
                    datesList.add(resourceBundle.getString("label.all.sale.dates"));
                    datesList.addAll(databaseService.findSaleDates());
                    sDate.setItems(FXCollections.observableArrayList(datesList));
                    sDate.getSelectionModel().selectFirst();
                    namesList = new ArrayList<>();
                    namesList.add(resourceBundle.getString("label.all.sale.names"));
                    namesList.addAll(databaseService.findSaleNames());
                    sName.setItems(FXCollections.observableArrayList(namesList));
                    sName.getSelectionModel().selectFirst();
                }
        );

        sName.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            String d = sDate.getValue();
            String n = namesList.get(newValue.intValue());
            applyFilters(d,n);
        });
        sDate.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            String d = datesList.get(newValue.intValue());
            String n = sName.getValue();
            applyFilters(d,n);
        });
        totalPrice.setCellValueFactory(cell -> {
            DecimalFormat decimalFormat = new DecimalFormat("#.00");
            return new SimpleStringProperty(decimalFormat.format(cell.getValue().getWeight() * cell.getValue().getPrice()).replace(',', '.'));
        });
        //load dialog layout
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/dialog_sales.fxml"));
            fxmlLoader.setResources(ResourceBundle.getBundle("lang.file"));
            saleDialogFXML = fxmlLoader.load();
            dialogController = fxmlLoader.getController();
        } catch (IOException e) {
            saleDialogFXML = new HBox();
        }
        //Open dialog and wait
        addSale.setOnAction(event -> {
            saleDialog = new Dialog<>();
            saleDialog.setTitle(resourceBundle.getString("label.user.new"));
            saleDialog.setResizable(true);
            //close dialog window with close button
            Window window = saleDialog.getDialogPane().getScene().getWindow();
            window.setOnCloseRequest(e -> saleDialog.close());
            saleDialog.getDialogPane().setContent(saleDialogFXML);
            ButtonType submitButton = new ButtonType(resourceBundle.getString("label.add"), ButtonBar.ButtonData.OK_DONE);
            saleDialog.getDialogPane().getButtonTypes().add(submitButton);
            //validate entered data
            Button okButton = (Button) saleDialog.getDialogPane().lookupButton(submitButton);
            okButton.getStylesheets().add(getClass().getResource("/fxml/style.css").toExternalForm());
            okButton.getStyleClass().add("action-button");
            okButton.addEventFilter(ActionEvent.ACTION, actionEvent -> {
                if (!dialogController.isValid()) {
                    actionEvent.consume();
                }
            });
            //
            saleDialog.setResultConverter(param -> {
                if (param == submitButton) {
                    return dialogController.getSales();
                }
                saleDialog.close();
                return null;
            });
            Optional<AgriculturalSale> response = saleDialog.showAndWait();

            if (response.isPresent()) {
                AgriculturalSale save = databaseService.save(response.get());
                if (save != null) salesTable.getItems().add(save);
            }
        });

        //export to excel file
        excelSale.setOnAction(event -> {
            //select file location
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle(resourceBundle.getString("button.excel"));
            fileChooser.setInitialFileName(resourceBundle.getString("file.excel.sale"));
            FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Excel files", "*.xlsx");
            fileChooser.getExtensionFilters().add(extensionFilter);
            File file = fileChooser.showSaveDialog(excelSale.getScene().getWindow());
            if (file != null) {
                //create excel file
                try {
                    FileOutputStream outputStream = new FileOutputStream(file.getAbsolutePath());
                    XSSFWorkbook workbook = new XSSFWorkbook();
                    XSSFSheet sheet = workbook.createSheet(resourceBundle.getString("file.excel.sale"));
                    List<AgriculturalSale> list = salesTable.getItems();
                    int rowNum = 0;
                    Row row = sheet.createRow(rowNum++);
                    int colNum = 0;
                    Cell cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("table.sale.date"));
                    cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("table.sale.name"));
                    cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("table.sale.weight"));
                    cell = row.createCell(colNum);
                    cell.setCellValue(resourceBundle.getString("table.sale.price"));
                    sheet.createRow(rowNum++);
                    //build row for data
                    for (AgriculturalSale s : list) {
                        row = sheet.createRow(rowNum++);
                        colNum = 0;
                        cell = row.createCell(colNum++);
                        cell.setCellValue(s.getDate().toString());
                        cell = row.createCell(colNum++);
                        cell.setCellValue(s.getName());
                        cell = row.createCell(colNum++);
                        cell.setCellValue(s.getWeight());
                        cell = row.createCell(colNum);
                        cell.setCellValue(s.getPrice());
                    }
                    workbook.write(outputStream);
                    workbook.close();
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    System.err.println("Excel file is dead");
                    DiscordLogService.get().write("Sale Excel save error", LocalDateTime.now(), e);
                }
            }
        });
    }
}
