package controllers.browsers;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import model.AgroTechnicalTreatmentsRecord;
import model.EntityId;
import model.PlantProtectionRecord;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Details browser display controller.
 *
 * @author Dawid
 * @version 1
 * @since 2019-06-09
 */
public class DetailsBrowser implements Initializable {
    private ObservableList<EntityId> list;
    private int pos = 0;
    private Pane plantFXML, agroFXML;
    private DetailsAgro agroController;
    private DetailsPlant plantController;
    @FXML
    private BorderPane detailsBrowser;


    /**
     * Move to previous element and display it.
     *
     * @return true if switch to previous element successfully
     */
    public boolean previous() {
        if (list != null && pos > 0) {
            display(list.get(--pos));
            return true;
        }
        return false;
    }

    /**
     * Move to next element and display it.
     *
     * @return true if switch to next element successfully
     */
    public boolean next() {
        if (list != null && pos < list.size() - 1) {
            display(list.get(++pos));
            return true;
        }
        return false;
    }

    /**
     * Attach list and set current position.
     *
     * @param list the list
     * @param pos  the position selected element
     */
    public void setList(ObservableList<EntityId> list ,int pos) {
        this.pos = pos;
        this.list = list;
        if (list != null && !list.isEmpty()) {
            System.err.println("List is not empty");
            if (list.get(pos) instanceof AgroTechnicalTreatmentsRecord) {
                System.err.println("AgroTechnicalTreatmentsRecord");
                detailsBrowser.setCenter(agroFXML);
                display(list.get(pos));
            } else if (list.get(pos) instanceof PlantProtectionRecord) {
                System.err.println("PlantProtectionRecord");
                detailsBrowser.setCenter(plantFXML);
                display(list.get(pos));
            }
        }
    }
    private void display(AgroTechnicalTreatmentsRecord record){
        if(agroController!=null) {
            agroController.display(record);
        }
    }
    private void display(PlantProtectionRecord record){
        if(plantController!=null){
            plantController.display(record);
        }
    }
    private void display(EntityId record){
        if(record instanceof AgroTechnicalTreatmentsRecord){
            display((AgroTechnicalTreatmentsRecord) record);
        } else if(record instanceof PlantProtectionRecord){
            display((PlantProtectionRecord) record);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/details_plant.fxml"));
            fxmlLoader.setResources(resources.getBundle("lang.file"));
            plantFXML = fxmlLoader.load();
            plantController=fxmlLoader.getController();
            fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/details_agro.fxml"));
            fxmlLoader.setResources(resources.getBundle("lang.file"));
            agroFXML = fxmlLoader.load();
            agroController = fxmlLoader.getController();
        } catch (IOException e) {
            agroFXML = new GridPane();
            plantFXML = new GridPane();
        }
    }
}
