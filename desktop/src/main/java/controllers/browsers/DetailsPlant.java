package controllers.browsers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import model.PlantProtectionRecord;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Plant details controller.
 *
 * @author Dawid
 * @version 1
 * @since 2019-06-09
 */
public class DetailsPlant implements Initializable {
    private ResourceBundle resourceBundle;
    @FXML
    private Label date;
    // plant name
    @FXML
    private Label plant;
    // total crop area
    @FXML
    private Label totalArea;
    // area of the field where the treatment was performed
    @FXML
    private Label treatmentArea;
    // area name or number
    @FXML
    private Label areaName;
    // name of the plant protection product
    @FXML
    private Label protectionName;
    // the dose of the plant protection product liter or kg or %
    @FXML
    private Label protectionDose;
    // the dose of liquid
    @FXML
    private Label liquidDose;
    // the reason for using a plant protection product
    @FXML
    private Label reasonUse;
    @FXML
    private TextArea comments;
    @FXML
    private Label user;

    /**
     * Get record and set correspond labels and fields.
     *
     * @param record the record
     */
    public void display(PlantProtectionRecord record) {
        date.setText(record.getDate().toString());
        plant.setText(record.getPlant());
        totalArea.setText(record.getTotalArea().toString());
        treatmentArea.setText(record.getTreatmentArea().toString()+" "+resourceBundle.getString("label.plant.out.of"));
        areaName.setText(record.getAreaName());
        protectionName.setText(record.getProtectionName());
        protectionDose.setText(record.getProtectionDose().toString()+ " "+resourceBundle.getString("label.dose.protection"));
        liquidDose.setText(record.getLiquidDose().toString() + " " +resourceBundle.getString("label.dose.liquid"));
        reasonUse.setText(record.getReasonUse());
        comments.setText(record.getComments());
        user.setText(record.getUser().getFirstName() + " " + record.getUser().getLastName());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resourceBundle=resources;


    }
}
