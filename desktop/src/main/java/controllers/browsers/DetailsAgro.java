package controllers.browsers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import model.AgroTechnicalTreatmentsRecord;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Agrotechnical details controller.
 *
 * @author Dawid
 * @version 1
 * @since 2019-06-09
 */
public class DetailsAgro  implements Initializable {
    private ResourceBundle resourceBundle;
    @FXML
    private Label date;
    @FXML
    private Label species;
    @FXML
    private Label totalArea;
    @FXML
    private Label treatmentArea;
    @FXML
    private Label fertilizerType;
    @FXML
    private Label fertilizerDose;
    @FXML
    private Label fertilizerDoseOnArea;

    /**
     * Get record and set correspond labels and fields.
     *
     * @param record the record
     */
    public void display(AgroTechnicalTreatmentsRecord record) {
        date.setText(record.getDate().toString());
        species.setText(record.getSpecies());
        totalArea.setText(record.getTotalArea().toString());
        treatmentArea.setText(record.getTreatmentArea().toString()+" "+resourceBundle.getString("label.agro.out.of"));
        fertilizerType.setText(record.getFertilizerType());
        fertilizerDose.setText(record.getFertilizerDose().toString()+" "+resourceBundle.getString("label.agro.hg.ha"));
        fertilizerDoseOnArea.setText(record.getFertilizerDoseOnArea().toString() +" "+resourceBundle.getString("label.agro.per.area"));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resourceBundle =resources;
    }
}
