package controllers;

import controllers.browsers.DetailsBrowser;
import controllers.dialogs.PlantsDialogCommentsController;
import controllers.dialogs.PlantsDialogController;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import javafx.util.Callback;
import model.PlantProtectionRecord;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import services.DatabaseService;
import services.DiscordLogService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * The type Users controller.
 *
 * @author Dawid
 * @version 2
 * @since 2019-04-05
 */
public class PlantsController implements Initializable {
    private DatabaseService databaseService;
    @FXML
    private TableView<PlantProtectionRecord> plantsTable;
    @FXML
    private TableColumn<PlantProtectionRecord, String> user;
    @FXML
    private TableColumn<PlantProtectionRecord, String> comments;
    @FXML
    private Button addPPR;
    @FXML
    private Button excelPPR;

    private PlantsDialogController dialogController;
    private Pane plantsDialogFXML;
    private Dialog<PlantProtectionRecord> plantsDialog;
    private PlantsDialogCommentsController commentsController;
    private Pane commentsDialogFXML;
    private Dialog<String> commentsDialog;
    private ResourceBundle resourceBundle;


    /**
     * User observable list observable list.
     *
     * @return the observable list
     */
    public ObservableList<PlantProtectionRecord> observableList() {
        ObservableList<PlantProtectionRecord> list = FXCollections.observableList(databaseService.findAll(PlantProtectionRecord.class));
        return list;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resourceBundle = resources;
        this.databaseService = DatabaseService.getDB();
        Platform.runLater(() -> plantsTable.setItems(observableList()));
        //load dialog layout
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/dialog_plants.fxml"));
            fxmlLoader.setResources(ResourceBundle.getBundle("lang.file"));
            plantsDialogFXML = fxmlLoader.load();
            dialogController = fxmlLoader.getController();
            fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/dialog_plants_comment.fxml"));
            fxmlLoader.setResources(ResourceBundle.getBundle("lang.file"));
            commentsDialogFXML = fxmlLoader.load();
            commentsController = fxmlLoader.getController();
        } catch (IOException e) {
            plantsDialogFXML = new HBox();
            commentsDialogFXML = new HBox();
        }
        user.setCellValueFactory(cell -> new SimpleStringProperty(cell.getValue().getUser().getFirstName() + " " + cell.getValue().getUser().getLastName()));
        comments.setCellFactory(new Callback<TableColumn<PlantProtectionRecord, String>, TableCell<PlantProtectionRecord, String>>() {
            @Override
            public TableCell<PlantProtectionRecord, String> call(TableColumn<PlantProtectionRecord, String> param) {
                TableCell<PlantProtectionRecord, String> cell = new TableCell<PlantProtectionRecord, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            if (item.length() > 20) {
                                setText(item.substring(0, 20) + "...");
                            } else {
                                setText(item);
                            }
                            Tooltip tooltip = new Tooltip();
                            tooltip.setText(item);
                            tooltip.setAutoHide(false);
                            setTooltip(tooltip);
                        }
                    }
                };
                cell.setOnMouseClicked(event -> {
                    commentsController.displayComments(((PlantProtectionRecord) cell.getTableRow().getItem()).getComments());
                    commentsDialog = new Dialog<>();
                    commentsDialog.setTitle(null);
                    commentsDialog.setGraphic(null);
                    commentsDialog.getDialogPane().setContent(commentsDialogFXML);
                    Window window = commentsDialog.getDialogPane().getScene().getWindow();
                    window.setOnCloseRequest(e -> commentsDialog.close());
                    commentsDialog.show();
                });
                return cell;
            }
        });

        //  Open dialog and wait
        addPPR.setOnAction(event -> {
            ////
            plantsDialog = new Dialog<>();
            plantsDialog.setTitle(resourceBundle.getString("label.record.new"));
            plantsDialog.setResizable(true);
            //close dialog window with close button
            Window window = plantsDialog.getDialogPane().getScene().getWindow();
            window.setOnCloseRequest(e -> plantsDialog.close());
            plantsDialog.getDialogPane().setContent(plantsDialogFXML);
            ButtonType submitButton = new ButtonType(resourceBundle.getString("label.add"), ButtonBar.ButtonData.OK_DONE);
            plantsDialog.getDialogPane().getButtonTypes().add(submitButton);
            //validate entered data
            Button okButton = (Button) plantsDialog.getDialogPane().lookupButton(submitButton);
            okButton.getStylesheets().add(getClass().getResource("/fxml/style.css").toExternalForm());
            okButton.getStyleClass().add("action-button");
            okButton.addEventFilter(ActionEvent.ACTION, actionEvent -> {
                if (!dialogController.isValid()) {
                    actionEvent.consume();
                }
            });
            plantsDialog.setResultConverter(param -> {
                if (param == submitButton) {
                    return dialogController.getRecord();
                }
                plantsDialog.close();
                return null;
            });
            Optional<PlantProtectionRecord> response = plantsDialog.showAndWait();

            if (response.isPresent()) {
                PlantProtectionRecord save = databaseService.save(response.get());
                if (save != null) plantsTable.getItems().add(save);

            }
        });
        //Remove user action for content menu
        MenuItem removeMI = new MenuItem(resourceBundle.getString("label.remove"));
        removeMI.setOnAction(event -> {
            int i = plantsTable.getSelectionModel().getSelectedIndex();
            PlantProtectionRecord toRemove = plantsTable.getItems().get(i);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(null);
            alert.setContentText(resourceBundle.getString("label.remove.record")+" "+ toRemove.getPlant() + " - " + toRemove.getProtectionName() + " - " + toRemove.getReasonUse());
            alert.setHeaderText(null);
            alert.setGraphic(null);
            ((Button) alert.getDialogPane().lookupButton(ButtonType.OK)).setText(resourceBundle.getString("label.yes"));
            ((Button) alert.getDialogPane().lookupButton(ButtonType.CANCEL)).setText(resourceBundle.getString("label.no"));
            Optional<ButtonType> resp = alert.showAndWait();
            if (resp.get() == ButtonType.OK) {
                boolean s = databaseService.remove(PlantProtectionRecord.class, toRemove);
                if (s) plantsTable.getItems().remove(toRemove);
            }
        });
        MenuItem detailsMI = new MenuItem(resourceBundle.getString("label.details"));
        detailsMI.setOnAction(event -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/details_browser.fxml"));
                fxmlLoader.setResources(ResourceBundle.getBundle("lang.file"));
                Pane detailsFXML = fxmlLoader.load();
                DetailsBrowser detailsBrowser = fxmlLoader.getController();
                detailsBrowser.setList(FXCollections.observableArrayList(plantsTable.getItems()),plantsTable.getSelectionModel().getFocusedIndex());
                //  commentsController.displayComments(((PlantProtectionRecord) cell.getTableRow().getItem()).getComments());
                Dialog<Boolean> detailsDialog = new Dialog<>();
                detailsDialog.setTitle(null);
                detailsDialog.setGraphic(null);
                detailsDialog.setResizable(true);
                detailsDialog.getDialogPane().setContent(detailsFXML);
//                detailsDialog.getDialogPane().setMinSize(500,300);
                Window window = detailsDialog.getDialogPane().getScene().getWindow();
                window.setOnCloseRequest(e -> detailsDialog.close());
                detailsDialog.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        ContextMenu menu = new ContextMenu();
        menu.getItems().addAll(removeMI,detailsMI);
        plantsTable.setContextMenu(menu);
        //export to excel file
        excelPPR.setOnAction(event -> {
            //select file location
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle(resourceBundle.getString("button.excel"));
            fileChooser.setInitialFileName(resourceBundle.getString("file.excel.plant"));
            FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Excel files", "*.xlsx");
            fileChooser.getExtensionFilters().add(extensionFilter);

            File file = fileChooser.showSaveDialog(excelPPR.getScene().getWindow());
            if (file != null) {
                //create excel file
                try {
                    FileOutputStream outputStream = new FileOutputStream(file.getAbsolutePath());
                    XSSFWorkbook workbook = new XSSFWorkbook();
                    XSSFSheet sheet = workbook.createSheet(resourceBundle.getString("file.excel.plant"));
                    List<PlantProtectionRecord> list = plantsTable.getItems();
                    int rowNum = 0;
                    Row row = sheet.createRow(rowNum++);
                    int colNum = 0;
                    Cell cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("table.plant.date"));
                    cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("table.plant.user"));
                    cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("table.plant.name"));
                    cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("table.plant.area.name"));
                    cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("table.plant.area.treatment"));
                    cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("table.plant.area.total"));
                    cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("dialog.plant.protection.name"));
                    cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("dialog.plant.protection.dose"));
                    cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("table.plant.liquid"));
                    cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("table.plant.reason.use"));
                    cell = row.createCell(colNum);
                    cell.setCellValue(resourceBundle.getString("table.plant.comments"));
                    sheet.createRow(rowNum++);
                    //build row for data
                    for (PlantProtectionRecord p : list) {
                        row = sheet.createRow(rowNum++);
                        colNum = 0;
                        cell = row.createCell(colNum++);
                        cell.setCellValue(p.getDate().toString());
                        cell = row.createCell(colNum++);
                        cell.setCellValue(p.getUser().getFirstName() + " " + p.getUser().getLastName());
                        cell = row.createCell(colNum++);
                        cell.setCellValue(p.getPlant());
                        cell = row.createCell(colNum++);
                        cell.setCellValue(p.getAreaName());
                        cell = row.createCell(colNum++);
                        cell.setCellValue(p.getTreatmentArea().toString());
                        cell = row.createCell(colNum++);
                        cell.setCellValue(p.getTotalArea().toString());
                        cell = row.createCell(colNum++);
                        cell.setCellValue(p.getProtectionName());
                        cell = row.createCell(colNum++);
                        cell.setCellValue(p.getProtectionDose().toString());
                        cell = row.createCell(colNum++);
                        cell.setCellValue(p.getLiquidDose().toString());
                        cell = row.createCell(colNum++);
                        cell.setCellValue(p.getReasonUse());
                        cell = row.createCell(colNum);
                        cell.setCellValue(p.getComments());
                    }
                    workbook.write(outputStream);
                    workbook.close();
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    System.err.println("Excel file is dead");
                    DiscordLogService.get().write("Plant Excel save error", LocalDateTime.now(),e);
                }
            }
        });
    }
}
