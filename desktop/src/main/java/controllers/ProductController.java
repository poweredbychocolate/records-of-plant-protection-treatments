package controllers;

import controllers.dialogs.ProductDialogController;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Window;
import model.PlantProtectionProduct;
import services.DatabaseService;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * The type Product controller.
 * @author Dawid
 * @version 1
 */
public class ProductController implements Initializable {
    private DatabaseService databaseService;
    private ResourceBundle resourceBundle;
    @FXML
    private TableView<PlantProtectionProduct> productTable;
    @FXML
    private Button settings;
    @FXML
    private Button clear;
    @FXML
    private TextField sName;
    @FXML
    private ChoiceBox<String> sType;
    private Pane step1Pane;
    private ArrayList<String> types;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resourceBundle = resources;
        this.databaseService = DatabaseService.getDB();
        Platform.runLater(() -> {
            productTable.setItems(observableList());
            types = new ArrayList<>();
            types.add(resourceBundle.getString("label.all.product.types"));
            types.addAll(databaseService.findProductTypes());
            sType.setItems(FXCollections.observableArrayList(types));
            sType.getSelectionModel().selectFirst();
            clear.setOnAction(event -> {
                sType.getSelectionModel().selectFirst();
                sName.clear();
            });
        });
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/dialog_product_step1.fxml"));
            fxmlLoader.setResources(ResourceBundle.getBundle("lang.file"));
            step1Pane = fxmlLoader.load();
            ((ProductDialogController) fxmlLoader.getController()).setProductController(this);
        } catch (IOException e) {
            step1Pane = new VBox();
        }

        settings.setOnAction(event -> {
            Dialog<Boolean> loadDialog = new Dialog<>();
            loadDialog.setGraphic(null);
            loadDialog.setTitle(resourceBundle.getString("label.product.setting"));
            Window window = loadDialog.getDialogPane().getScene().getWindow();
            window.setOnCloseRequest(closeEvent -> loadDialog.close());
            loadDialog.getDialogPane().setContent(step1Pane);
            loadDialog.show();
        });
        sType.selectionModelProperty().addListener((observable, oldValue, newValue) -> System.err.println(newValue));
        sType.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if(types.get(newValue.intValue()).equals(resourceBundle.getString("label.all.product.types"))){
                productTable.setItems(observableList(sName.getText(),null));
            }else {
                productTable.setItems(observableList(sName.getText(),types.get(newValue.intValue())));
            }
        });
        sName.textProperty().addListener((observable, oldValue, newValue) -> {
            String t=null;
            if(!types.get(sType.getSelectionModel().getSelectedIndex()).equals(resourceBundle.getString("label.all.product.types"))){
                t=types.get(sType.getSelectionModel().getSelectedIndex());
            }
            if(!sName.getText().equals("")){
                productTable.setItems(observableList(sName.getText(),t));
            }
            else {
                productTable.setItems(observableList(null,t));
            }
        });

    }

    private ObservableList<PlantProtectionProduct> observableList() {
        ObservableList<PlantProtectionProduct> list = FXCollections.observableList(databaseService.findAllProduct());
        return list;
    }
    private ObservableList<PlantProtectionProduct> observableList(String name,String type) {
        ObservableList<PlantProtectionProduct> list = FXCollections.observableList(databaseService.findAllProduct(name,type));
        return list;
    }

    /**
     * Refresh product table.
     */
    public void refresh() {
        Platform.runLater(() -> productTable.setItems(observableList()));
    }
}
