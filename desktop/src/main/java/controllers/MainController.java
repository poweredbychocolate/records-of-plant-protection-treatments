package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import model.User;
import model.Validator;
import services.DatabaseService;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

/**
 * The type Main controller.
 *
 * @author Dawid
 * @version 4
 * @since 2019-03-31
 */
public class MainController implements Initializable {
    @FXML
    private Button menu1;
    @FXML
    private Button menu2;
    @FXML
    private Button menu3;
    @FXML
    private Button menu4;
    @FXML
    private Button menu5;
    @FXML
    private BorderPane mainContent;

    private Pane alertPane;
    private AlertControler alertControler;
    private final String menuStyle = "menu-button";
    private final String selectedMenuStyle = "selected-menu-button";


    /**
     * Menu action, replace context pane in main window
     *
     * @param event the event
     */
    @FXML
    void menuAction(ActionEvent event) {
        Button menuButton = (Button) event.getSource();
        if (menuButton.equals(menu1)) {
            menu2.getStyleClass().remove(selectedMenuStyle);
            menu3.getStyleClass().remove(selectedMenuStyle);
            menu4.getStyleClass().remove(selectedMenuStyle);
            menu5.getStyleClass().remove(selectedMenuStyle);
            menu1.getStyleClass().remove(menuStyle);
            menu2.getStyleClass().remove(menuStyle);
            menu3.getStyleClass().remove(menuStyle);
            menu4.getStyleClass().remove(menuStyle);
            menu4.getStyleClass().remove(menuStyle);
            menu1.getStyleClass().add(selectedMenuStyle);
            menu2.getStyleClass().add(menuStyle);
            menu3.getStyleClass().add(menuStyle);
            menu4.getStyleClass().add(menuStyle);
            menu5.getStyleClass().add(menuStyle);
            loadPlantProtectionContent();
        } else if (menuButton.equals(menu2)) {
            menu1.getStyleClass().remove(selectedMenuStyle);
            menu3.getStyleClass().remove(selectedMenuStyle);
            menu4.getStyleClass().remove(selectedMenuStyle);
            menu5.getStyleClass().remove(selectedMenuStyle);
            menu1.getStyleClass().remove(menuStyle);
            menu2.getStyleClass().remove(menuStyle);
            menu3.getStyleClass().remove(menuStyle);
            menu4.getStyleClass().remove(menuStyle);
            menu5.getStyleClass().remove(menuStyle);
            menu1.getStyleClass().add(menuStyle);
            menu2.getStyleClass().add(selectedMenuStyle);
            menu3.getStyleClass().add(menuStyle);
            menu4.getStyleClass().add(menuStyle);
            menu5.getStyleClass().add(menuStyle);
            loadUsersContent();
        } else if (menuButton.equals(menu3)) {
            menu1.getStyleClass().remove(selectedMenuStyle);
            menu2.getStyleClass().remove(selectedMenuStyle);
            menu4.getStyleClass().remove(selectedMenuStyle);
            menu5.getStyleClass().remove(selectedMenuStyle);
            menu1.getStyleClass().remove(menuStyle);
            menu2.getStyleClass().remove(menuStyle);
            menu3.getStyleClass().remove(menuStyle);
            menu4.getStyleClass().remove(menuStyle);
            menu5.getStyleClass().remove(menuStyle);
            menu1.getStyleClass().add(menuStyle);
            menu2.getStyleClass().add(menuStyle);
            menu3.getStyleClass().add(selectedMenuStyle);
            menu4.getStyleClass().add(menuStyle);
            menu5.getStyleClass().add(menuStyle);
            loadAgroTechnicalContent();
        } else if (menuButton.equals(menu4)) {
            menu1.getStyleClass().remove(selectedMenuStyle);
            menu2.getStyleClass().remove(selectedMenuStyle);
            menu3.getStyleClass().remove(selectedMenuStyle);
            menu5.getStyleClass().remove(selectedMenuStyle);
            menu1.getStyleClass().remove(menuStyle);
            menu2.getStyleClass().remove(menuStyle);
            menu3.getStyleClass().remove(menuStyle);
            menu4.getStyleClass().remove(menuStyle);
            menu5.getStyleClass().remove(menuStyle);
            menu1.getStyleClass().add(menuStyle);
            menu2.getStyleClass().add(menuStyle);
            menu3.getStyleClass().add(menuStyle);
            menu4.getStyleClass().add(selectedMenuStyle);
            menu5.getStyleClass().add(menuStyle);
            loadPlantProductContent();
        }else if (menuButton.equals(menu5)) {
            menu1.getStyleClass().remove(selectedMenuStyle);
            menu2.getStyleClass().remove(selectedMenuStyle);
            menu3.getStyleClass().remove(selectedMenuStyle);
            menu4.getStyleClass().remove(selectedMenuStyle);
            menu1.getStyleClass().remove(menuStyle);
            menu2.getStyleClass().remove(menuStyle);
            menu3.getStyleClass().remove(menuStyle);
            menu4.getStyleClass().remove(menuStyle);
            menu5.getStyleClass().remove(menuStyle);
            menu1.getStyleClass().add(menuStyle);
            menu2.getStyleClass().add(menuStyle);
            menu3.getStyleClass().add(menuStyle);
            menu4.getStyleClass().add(menuStyle);
            menu5.getStyleClass().add(selectedMenuStyle);
            loadSales();
        }
    }

    private void checkExpiring() {
        LocalDate localDate = LocalDate.now().minusYears(Validator.PLANT_PROTECTION_VALIDITY_YEARS).plusDays(14);
        List<User> expiringlist = DatabaseService.getDB().findExpiringCertificates(localDate);
        if (expiringlist.size() > 0) {
            mainContent.setTop(null);
            mainContent.setTop(alertPane);
            alertControler.displayExpiring(expiringlist);
        }
    }

    /**
     * Load pane with users and place his in main context
     */
    private void loadUsersContent() {
        try {
            mainContent.setCenter(null);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/users_fragment.fxml"));
            fxmlLoader.setResources(ResourceBundle.getBundle("lang.file"));
            Pane loadPane = fxmlLoader.load();
            mainContent.setCenter(loadPane);
        } catch (IOException e) {
            System.err.println("User Pane is dead");
        }
    }

    private void loadPlantProtectionContent() {
        try {
            mainContent.setCenter(null);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/plant_fragment.fxml"));
            fxmlLoader.setResources(ResourceBundle.getBundle("lang.file"));
            Pane loadPane = fxmlLoader.load();
            mainContent.setCenter(loadPane);
        } catch (IOException e) {
            System.err.println("Plant Pane is dead");
        }
    }

    private void loadAgroTechnicalContent() {
        try {
            mainContent.setCenter(null);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/agro_technical_fragment.fxml"));
            fxmlLoader.setResources(ResourceBundle.getBundle("lang.file"));
            Pane loadPane = fxmlLoader.load();
            mainContent.setCenter(loadPane);
        } catch (IOException e) {
            System.err.println("Agro-technical treatments Pane is dead");
        }
    }

    private void loadPlantProductContent() {
        try {
            mainContent.setCenter(null);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/fragment_product.fxml"));
            fxmlLoader.setResources(ResourceBundle.getBundle("lang.file"));
            Pane loadPane = fxmlLoader.load();
            mainContent.setCenter(loadPane);
        } catch (IOException e) {
            System.err.println("Product Pane is dead");
        }
    }
    private void loadSales() {
        try {
            mainContent.setCenter(null);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/fragment_sales.fxml"));
            fxmlLoader.setResources(ResourceBundle.getBundle("lang.file"));
            Pane loadPane = fxmlLoader.load();
            mainContent.setCenter(loadPane);
        } catch (IOException e) {
            System.err.println("Sales Pane is dead");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/main_alert.fxml"));
            fxmlLoader.setResources(ResourceBundle.getBundle("lang.file"));
            alertPane = fxmlLoader.load();
            alertControler = fxmlLoader.getController();
            loadPlantProtectionContent();
            checkExpiring();
            menu1.getStyleClass().add(selectedMenuStyle);
            menu2.getStyleClass().add(menuStyle);
            menu3.getStyleClass().add(menuStyle);
            menu4.getStyleClass().add(menuStyle);
            menu5.getStyleClass().add(menuStyle);
        } catch (IOException e) {
            System.err.println("Main alert is dead");
        }
    }
}
