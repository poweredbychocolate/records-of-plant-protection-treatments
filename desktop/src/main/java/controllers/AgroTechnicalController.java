package controllers;

import controllers.browsers.DetailsBrowser;
import controllers.dialogs.AgroTechnicalDialogController;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import model.AgroTechnicalTreatmentsRecord;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import services.DatabaseService;
import services.DiscordLogService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * The type Agro technical controller.
 * @author Dawid
 * @version 2
 * @since 2019-05-08
 */
public class AgroTechnicalController implements Initializable {
    private DatabaseService databaseService;
    @FXML
    private TableView<AgroTechnicalTreatmentsRecord> agroTable;
    @FXML
    private Button addRecord;
    @FXML
    private Button excelRecord;

    private AgroTechnicalDialogController dialogController;
    private Pane agroDialogFXML;
    private Dialog<AgroTechnicalTreatmentsRecord> agroDialog;
    private ResourceBundle resourceBundle;


    /**
     * Observable list observable list.
     *
     * @return the observable list
     */
    public ObservableList<AgroTechnicalTreatmentsRecord> observableList() {
        ObservableList<AgroTechnicalTreatmentsRecord> list = FXCollections.observableList(databaseService.findAll(AgroTechnicalTreatmentsRecord.class));
        return list;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resourceBundle = resources;
        this.databaseService = DatabaseService.getDB();
        Platform.runLater(() -> agroTable.setItems(observableList()));
        //load dialog layout
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/dialog_agro_technical.fxml"));
            fxmlLoader.setResources(ResourceBundle.getBundle("lang.file"));
            agroDialogFXML = fxmlLoader.load();
            dialogController = fxmlLoader.getController();
        } catch (IOException e) {
            agroDialogFXML = new HBox();
        }

        //  Open dialog and wait
        addRecord.setOnAction(event -> {
            ////
            agroDialog = new Dialog<>();

            agroDialog.setTitle(resourceBundle.getString("label.record.new"));
            agroDialog.setResizable(true);
            //close dialog window with close button
            Window window = agroDialog.getDialogPane().getScene().getWindow();
            window.setOnCloseRequest(e -> agroDialog.close());
            agroDialog.getDialogPane().setContent(agroDialogFXML);
            ButtonType submitButton = new ButtonType(resourceBundle.getString("label.add"), ButtonBar.ButtonData.OK_DONE);
            agroDialog.getDialogPane().getButtonTypes().add(submitButton);
            //validate entered data
            Button okButton = (Button) agroDialog.getDialogPane().lookupButton(submitButton);
            okButton.getStylesheets().add(getClass().getResource("/fxml/style.css").toExternalForm());
            okButton.getStyleClass().add("action-button");
            okButton.addEventFilter(ActionEvent.ACTION, actionEvent -> {
                if (!dialogController.isValid()) {
                    actionEvent.consume();
                }
            });
            agroDialog.setResultConverter(param -> {
                if (param == submitButton) {
                    return dialogController.getRecord();
                }
                agroDialog.close();
                return null;
            });
            Optional<AgroTechnicalTreatmentsRecord> response = agroDialog.showAndWait();

            if (response.isPresent()) {
                AgroTechnicalTreatmentsRecord save = databaseService.save(response.get());
                if (save != null) agroTable.getItems().add(save);

            }
        });
        //Remove record action for content menu
        MenuItem removeMI = new MenuItem(resourceBundle.getString("label.remove"));
        removeMI.setOnAction(event -> {
            int i = agroTable.getSelectionModel().getSelectedIndex();
            AgroTechnicalTreatmentsRecord toRemove = agroTable.getItems().get(i);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(null);
            alert.setContentText(resourceBundle.getString("label.remove.record") + " " + toRemove.getSpecies() + " - " + toRemove.getDate());
            alert.setHeaderText(null);
            alert.setGraphic(null);
            ((Button) alert.getDialogPane().lookupButton(ButtonType.OK)).setText(resourceBundle.getString("label.yes"));
            ((Button) alert.getDialogPane().lookupButton(ButtonType.CANCEL)).setText(resourceBundle.getString("label.no"));
            Optional<ButtonType> resp = alert.showAndWait();
            if (resp.get() == ButtonType.OK) {
                boolean s = databaseService.remove(AgroTechnicalTreatmentsRecord.class, toRemove);
                if (s) agroTable.getItems().remove(toRemove);
            }
        });
        //Open details action
        MenuItem detailsMI = new MenuItem(resourceBundle.getString("label.details"));
        detailsMI.setOnAction(event -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/details_browser.fxml"));
                fxmlLoader.setResources(ResourceBundle.getBundle("lang.file"));
                Pane detailsFXML = fxmlLoader.load();
                DetailsBrowser detailsBrowser = fxmlLoader.getController();
                detailsBrowser.setList(FXCollections.observableArrayList(agroTable.getItems()),agroTable.getSelectionModel().getFocusedIndex());
              //  commentsController.displayComments(((PlantProtectionRecord) cell.getTableRow().getItem()).getComments());
                Dialog<Boolean> detailsDialog = new Dialog<>();
                detailsDialog.setTitle(null);
                detailsDialog.setGraphic(null);
                detailsDialog.setResizable(true);
                detailsDialog.getDialogPane().setContent(detailsFXML);
//                detailsDialog.getDialogPane().setMinSize(500,300);
                Window window = detailsDialog.getDialogPane().getScene().getWindow();
                window.setOnCloseRequest(e -> detailsDialog.close());
                detailsDialog.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        ContextMenu menu = new ContextMenu();
        menu.getItems().addAll(removeMI,detailsMI);
        agroTable.setContextMenu(menu);
        //export to excel file
        excelRecord.setOnAction(event -> {
            //select file location
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle(resourceBundle.getString("button.excel"));
            fileChooser.setInitialFileName(resourceBundle.getString("file.excel.agro"));
            FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Excel files", "*.xlsx");
            fileChooser.getExtensionFilters().add(extensionFilter);
            File file = fileChooser.showSaveDialog(excelRecord.getScene().getWindow());
            if (file != null) {
                //create excel file
                try {
                    FileOutputStream outputStream = new FileOutputStream(file.getAbsolutePath());
                    XSSFWorkbook workbook = new XSSFWorkbook();
                    XSSFSheet sheet = workbook.createSheet(resourceBundle.getString("file.excel.agro"));
                    List<AgroTechnicalTreatmentsRecord> list = agroTable.getItems();
                    int rowNum = 0;
                    Row row = sheet.createRow(rowNum++);
                    int colNum = 0;
                    Cell cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("table.agro.date"));
                    cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("dialog.agro.species"));
                    cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("table.agro.area"));
                    cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("table.agro.area.treatment"));
                    cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("table.agro.fertilizer.type"));
                    cell = row.createCell(colNum++);
                    cell.setCellValue(resourceBundle.getString("table.agro.dose"));
                    cell = row.createCell(colNum);
                    cell.setCellValue(resourceBundle.getString("dialog.agro.dose.on.area"));
                    sheet.createRow(rowNum++);
                    //build row for data
                    for (AgroTechnicalTreatmentsRecord a : list) {
                        row = sheet.createRow(rowNum++);
                        colNum = 0;
                        cell = row.createCell(colNum++);
                        cell.setCellValue(a.getDate().toString());
                        cell = row.createCell(colNum++);
                        cell.setCellValue(a.getSpecies());
                        cell = row.createCell(colNum++);
                        cell.setCellValue(a.getTotalArea().toString());
                        cell = row.createCell(colNum++);
                        cell.setCellValue(a.getTreatmentArea().toString());
                        cell = row.createCell(colNum++);
                        cell.setCellValue(a.getFertilizerType());
                        cell = row.createCell(colNum++);
                        cell.setCellValue(a.getFertilizerDose().toString());
                        cell = row.createCell(colNum);
                        cell.setCellValue(a.getFertilizerDoseOnArea().toString());
                    }
                    workbook.write(outputStream);
                    workbook.close();
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    DiscordLogService.get().write("Argro technical Excel save error", LocalDateTime.now(),e);
                    System.err.println("Excel file is dead");
                }
            }
        });
    }
}
