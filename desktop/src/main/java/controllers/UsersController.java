package controllers;

import controllers.dialogs.UserDialogController;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Window;
import model.User;
import model.Validator;
import services.DatabaseService;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * The type Users controller.
 *
 * @author Dawid
 * @version 1
 * @since 2019-04-05
 */
public class UsersController implements Initializable {
    private DatabaseService databaseService;
    @FXML
    private TableView<User> usersTable;
    @FXML
    private Button addUser;
    private Pane userDialogFXML;
    private Dialog<User> userDialog;
    private UserDialogController dialogController;
    private ResourceBundle resourceBundle;


    /**
     * User observable list observable list.
     *
     * @return the observable list
     */
    public ObservableList<User> userObservableList() {
        ObservableList<User> list = FXCollections.observableList(databaseService.findAll(User.class));
        return list;
    }

    /**
     * 18 n type property simple string property.
     *
     * @param type the type
     * @return the simple string property
     */
    public SimpleStringProperty i18nTypeProperty(String type) {
        if (type.equals(User.TYPE_PLANT_PROTECTION))
            return new SimpleStringProperty(resourceBundle.getString("label.plant.protection"));
        return new SimpleStringProperty(type);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resourceBundle = resources;
        this.databaseService = DatabaseService.getDB();
        Platform.runLater(() -> usersTable.setItems(userObservableList()));
//        userType.setCellValueFactory(cell -> i18nTypeProperty(cell.getValue().getType()));
        usersTable.setRowFactory(row -> new TableRow<User>() {
            @Override
            protected void updateItem(User item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty) {
                    if (item.getObtainingDate().isAfter(LocalDate.now().minusYears(Validator.PLANT_PROTECTION_VALIDITY_YEARS).plusDays(14)))
                        this.setStyle("");
                    else if (item.getObtainingDate().isBefore(LocalDate.now().minusYears(Validator.PLANT_PROTECTION_VALIDITY_YEARS)))
                        this.setStyle("-fx-background-color:red");
                    else if (item.getObtainingDate().isBefore(LocalDate.now().minusYears(Validator.PLANT_PROTECTION_VALIDITY_YEARS).plusDays(14)))
                        this.setStyle("-fx-background-color:orange");
                } else {
                    this.setStyle("");
                }
            }
        });
        //load dialog layout
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/dialog_user.fxml"));
            fxmlLoader.setResources(ResourceBundle.getBundle("lang.file"));
            userDialogFXML = fxmlLoader.load();
            dialogController = fxmlLoader.getController();
        } catch (IOException e) {
            userDialogFXML = new HBox();
        }

        //Open dialog and wait
        addUser.setOnAction(event -> {
            userDialog = new Dialog<>();
            userDialog.setTitle(resourceBundle.getString("label.user.new"));
            userDialog.setResizable(true);
            //close dialog window with close button
            Window window = userDialog.getDialogPane().getScene().getWindow();
            window.setOnCloseRequest(e -> userDialog.close());
            userDialog.getDialogPane().setContent(userDialogFXML);
            ButtonType submitButton = new ButtonType(resourceBundle.getString("label.add"), ButtonBar.ButtonData.OK_DONE);
            userDialog.getDialogPane().getButtonTypes().add(submitButton);
            //validate entered data
            Button okButton = (Button) userDialog.getDialogPane().lookupButton(submitButton);
            okButton.getStylesheets().add(getClass().getResource("/fxml/style.css").toExternalForm());
            okButton.getStyleClass().add("action-button");
            okButton.addEventFilter(ActionEvent.ACTION, actionEvent -> {
                if (!dialogController.isValid()) {
                    actionEvent.consume();
                }
            });
            //
            userDialog.setResultConverter(param -> {
                if (param == submitButton) {
                    return dialogController.getUser();
                }
                userDialog.close();
                return null;
            });
            Optional<User> response = userDialog.showAndWait();

            if (response.isPresent()) {
                User save = databaseService.save(response.get());
                if (save != null) usersTable.getItems().add(save);

            }
        });
        //Remove user action for content menu
        MenuItem removeMI = new MenuItem(resourceBundle.getString("label.remove"));
        removeMI.setOnAction(event -> {
            int i = usersTable.getSelectionModel().getSelectedIndex();
            User toRemove = usersTable.getItems().get(i);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(null);
            alert.setContentText(resourceBundle.getString("label.remove.user") + " " + toRemove.getFirstName() + " " + toRemove.getLastName());
            alert.setHeaderText(null);
            alert.setGraphic(null);
            ((Button) alert.getDialogPane().lookupButton(ButtonType.OK)).setText(resourceBundle.getString("label.yes"));
            ((Button) alert.getDialogPane().lookupButton(ButtonType.CANCEL)).setText(resourceBundle.getString("label.no"));
            Optional<ButtonType> resp = alert.showAndWait();
            if (resp.get() == ButtonType.OK) {
                boolean s = databaseService.remove(User.class, toRemove);
                if (s) usersTable.getItems().remove(toRemove);
            }
        });
        //Extension of the certificate's validity content menu option
        MenuItem refreshMI = new MenuItem(resourceBundle.getString("label.certificate.refresh"));
        refreshMI.setOnAction(event -> {
            int i = usersTable.getSelectionModel().getSelectedIndex();
            User toRefresh = usersTable.getItems().get(i);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(null);
            alert.setContentText(resourceBundle.getString("label.certificate.refresh.info") + " " + toRefresh.getFirstName() + " " + toRefresh.getLastName());
            alert.setHeaderText(null);
            alert.setGraphic(null);
            ((Button) alert.getDialogPane().lookupButton(ButtonType.OK)).setText(resourceBundle.getString("label.yes"));
            ((Button) alert.getDialogPane().lookupButton(ButtonType.CANCEL)).setText(resourceBundle.getString("label.no"));
            Optional<ButtonType> resp = alert.showAndWait();
            if (resp.get() == ButtonType.OK) {
                toRefresh.setObtainingDate(LocalDate.now());
                User refreshUser = databaseService.save(toRefresh);
                usersTable.getItems().set(i, refreshUser);
            }
        });

        ContextMenu menu = new ContextMenu();
        menu.getItems().addAll(removeMI, refreshMI);
        usersTable.setContextMenu(menu);

    }
}
