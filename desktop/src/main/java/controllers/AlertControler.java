package controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import model.User;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * The type Alert controler, display alerts in main window
 *
 * @author Dawid
 * @version 1
 * @since 2019 -03-31
 */
public class AlertControler implements Initializable {
    private LinkedList<String> alertQueue;
    private ResourceBundle resourceBundle;
    @FXML
    private Label alertLabel;
    @FXML
    private Button alertClose;
    @FXML
    private HBox alertBox;

    /**
     * Instantiates a new Alert controler.
     */
    public AlertControler(){
        alertQueue = new LinkedList<>();

    }

    /**
     * Sets alert.
     *
     * @param alert the alert
     */
    public void setAlert(String alert) {
        alertQueue.offer(alert);
        if(alertLabel.getText().equals(""))displayAlert();
    }


    /**
     * Display alert that inform about expiring certificate.
     *
     * @param list the list
     */
    public void displayExpiring(List<User> list) {

        for (User item : list) {
            alertQueue.offer(resourceBundle.getString("alert.certificate.end")+" " + item.getFirstName() + " " + item.getLastName() + " " + item.getObtainingDate());
        }
        if(alertLabel.getText().equals(""))displayAlert();
    }

    private void displayAlert() {
        if (alertQueue.size() > 0) {
            alertLabel.setText(alertQueue.pop());
        }else {
            alertLabel.setText("");
            ((BorderPane)alertBox.getParent()).setTop(null);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resourceBundle=resources;
        alertClose.setOnAction(event -> displayAlert());
    }
}
