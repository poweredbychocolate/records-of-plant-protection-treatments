package controllers.dialogs;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import model.AgriculturalSale;
import model.Validator;
import services.DatabaseService;

import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * The type Sales dialog controller.
 * @author Dawid
 * @version 1
 */
public class SalesDialogController implements Initializable {
    @FXML
    private TextField name;
    @FXML
    private ListView<String> nameList;
    @FXML
    private TextField weight;
    @FXML
    private TextField price;
    @FXML
    private DatePicker datePicker;
    @FXML
    private Label labelName;
    @FXML
    private Label labelWeight;
    @FXML
    private Label labelPrice;

    private ResourceBundle resourceBundle;
    private DatabaseService databaseService;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resourceBundle = resources;
        databaseService = DatabaseService.getDB();
        datePicker.setValue(LocalDate.now());
        new NumberListener(price);
        new NumberListener(weight);
        Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        // Show SATURDAY painted blue
                        DayOfWeek day = DayOfWeek.from(item);
                        if (day == DayOfWeek.SATURDAY) {
                            this.setTextFill(Color.BLUE);
                        }
                        //Show SUNDAY painted red
                        if (day == DayOfWeek.SUNDAY) {
                            this.setTextFill(Color.RED);
                        }
                        //Disable future day cell
                        if (item.isAfter(LocalDate.now())) {
                            this.setDisable(true);
                        }
                    }
                };
            }
        };
        datePicker.setDayCellFactory(dayCellFactory);
        name.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue!=null&&!newValue.isEmpty()) {
                ObservableList<String> observableList = FXCollections.observableArrayList();
                observableList.add(newValue);
                observableList.addAll(databaseService.findSalesNames(newValue));
                nameList.setItems(observableList);
                nameList.getSelectionModel().selectFirst();
            } else {
                nameList.setItems(null);
            }
        });

    }

    /**
     * Gets sales.
     *
     * @return the sales
     */
    public AgriculturalSale getSales() {
        AgriculturalSale agriculturalSale = new AgriculturalSale();
        agriculturalSale.setDate(datePicker.getValue());
        agriculturalSale.setName(nameList.getSelectionModel().getSelectedItem());
        agriculturalSale.setPrice(Double.parseDouble(price.getText()));
        agriculturalSale.setWeight(Double.parseDouble(weight.getText()));
        return agriculturalSale;
    }

    /**
     * Is valid boolean.
     *
     * @return the boolean
     */
    public boolean isValid() {
        boolean valid = true;
        if (!Validator.isValid(name.getText())) {
            labelName.setStyle("-fx-text-fill: red");
            labelName.setText(resourceBundle.getString("table.sale.name") + " - " + resourceBundle.getString("error.field.empty"));
            valid = false;
        } else {
            labelName.setStyle("");
            labelName.setText(resourceBundle.getString("table.sale.name"));
        }
        return valid;
    }
}

