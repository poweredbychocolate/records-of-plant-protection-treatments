package controllers.dialogs;


import app.PageLoader;
import controllers.ProductController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.Pane;
import javafx.stage.*;
import model.PlantProtectionProduct;
import services.DatabaseService;
import services.DiscordLogService;
import services.RecordFileService;

import java.io.*;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * The type Product dialog controller.
 * @version 1
 * @author Dawid
 */
public class ProductDialogController implements Initializable {
    private DatabaseService databaseService;
    private RecordFileService recordFileService;
    private ProductController productController;
    @FXML
    private Hyperlink link;
    @FXML
    private Button loadButton;
    @FXML
    private ProcessingDialogController processingController;
    private ResourceBundle resourceBundle;
    private Pane processingDialogFXML;
    private Scene processingScene;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resourceBundle = resources;
        this.databaseService = DatabaseService.getDB();
        this.recordFileService = new RecordFileService();
        link.setOnAction(event -> PageLoader.open(link));
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/box_processing.fxml"));
            fxmlLoader.setResources(ResourceBundle.getBundle("lang.file"));
            processingDialogFXML = fxmlLoader.load();
            processingController = fxmlLoader.getController();
            processingScene = new Scene(processingDialogFXML);
        } catch (IOException e) {
            System.err.println("Processing dialog is dead");
        }
        loadButton.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle(resourceBundle.getString("button.excel"));
            FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Excel files", "*.xls");
            fileChooser.getExtensionFilters().add(extensionFilter);
            File file = fileChooser.showOpenDialog(loadButton.getScene().getWindow());
            if (file != null) {
                loadButton.getScene().getWindow().hide();
                try {
                    List<PlantProtectionProduct> rowList = recordFileService.readPlantProtectionProducts(new FileInputStream(file));
                    ////
                    List<PlantProtectionProduct> productList = databaseService.findAll(PlantProtectionProduct.class);
                    if (rowList.size() > 0 && productList.size() > 0) {
                        // check if product list can be updated
                        List<PlantProtectionProduct> toUpdate = new ArrayList<>();
                        for (PlantProtectionProduct row : rowList) {
                            Optional<PlantProtectionProduct> tmp = productList.stream().filter(p -> p.getAuthorizationNumber().equals(row.getAuthorizationNumber()) && p.getName().equals(row.getName()) && p.getManufacturer().equals(row.getManufacturer())).findFirst();
                            if (tmp.isPresent()) {
                                //update
                                PlantProtectionProduct local = tmp.get();
                                local.setType(row.getType());
                                local.setRegisteredOffice(row.getRegisteredOffice());
                                local.setActiveSubstance(row.getActiveSubstance());
                                local.setClassificationHumanHealth(row.getClassificationHumanHealth());
                                local.setClassificationBeesHazard(row.getClassificationBeesHazard());
                                local.setClassificationAquaticOrganisms(row.getClassificationAquaticOrganisms());
                                local.setClassificationEnvironmentalHazards(row.getClassificationEnvironmentalHazards());
                                local.setValidityTerm(row.getValidityTerm());
                                local.setSalePeriod(row.getSalePeriod());
                                local.setUsePeriod(row.getUsePeriod());
                                local.setPackagingType(row.getPackagingType());
                                toUpdate.add(local);
                            } else {
                                //add to current list new protection product
                                toUpdate.add(row);
                            }
                        }
                        databaseService.save(toUpdate);

                    } else {
                    //if list is empty just save product from excel file
                        databaseService.save(rowList);
                        DiscordLogService.get().write("Plant Protection was loaded", LocalDateTime.now(),"First time someone load plant protection from excel file");
                    }
                    productController.refresh();
                } catch (Exception e) {
                    System.err.println("Excel Register file is trash");
                    DiscordLogService.get().write("Excel Register file error", LocalDateTime.now(),e);
                }
            }
        });

    }

    /**
     * Sets product controller.
     *
     * @param productController the product controller
     */
    public void setProductController(ProductController productController) {
        this.productController = productController;
    }
}
