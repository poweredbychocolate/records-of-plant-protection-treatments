package controllers.dialogs;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

/**
 * Number listener allow enter only double value.
 * @author Dawid
 * @version 1
 * @since 2019-05-08
 */
public class NumberListener implements ChangeListener<String> {
    private TextField field;

    /**
     * Instantiates a new Number listener.
     *
     * @param field the field
     */
    public NumberListener(TextField field) {
        this.field = field;
        this.field.textProperty().addListener(this);
        this.field.setText(Double.toString(0.0));
    }

    @Override
    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        if (!newValue.matches("\\d{0,20}([\\.]\\d{0,2})?")) {
            field.setText(oldValue);
        }
    }
}