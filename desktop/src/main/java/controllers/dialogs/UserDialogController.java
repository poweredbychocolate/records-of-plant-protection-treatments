package controllers.dialogs;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import model.User;
import model.Validator;

import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * The type User dialog controller. Get entered data from new user form an create user instance.
 *
 * @author Dawid
 * @version 1
 * @since 2019 -04-05
 */
public class UserDialogController implements Initializable {
    @FXML
    private TextField userFirstName;
    @FXML
    private TextField userLastName;
    @FXML
    private DatePicker datePicker;
    @FXML
    private Label labelFirstName;
    @FXML
    private Label labelLastName;

    private ResourceBundle resourceBundle;

    /**
     * Crete new {@link User} using form data
     *
     * @return the user
     */
    public User getUser() {

        User user = new User();
        user.setType(User.TYPE_PLANT_PROTECTION);
        user.setFirstName(userFirstName.getText());
        user.setLastName(userLastName.getText());
        user.setObtainingDate(datePicker.getValue());
        userLastName.clear();
        userFirstName.clear();
        datePicker.setValue(LocalDate.now());
        return user;

    }

    /**
     * Return true if fields data is correct
     *
     * @return the boolean
     */
    public boolean isValid() {
        boolean valid = true;
        if (!Validator.isValid(userFirstName.getText())) {
            labelFirstName.setStyle("-fx-text-fill: red");
            labelFirstName.setText(resourceBundle.getString("table.user.first.name")+" - "+resourceBundle.getString("error.field.empty"));
            valid=false;
        }else {
            labelFirstName.setStyle("");
            labelFirstName.setText(resourceBundle.getString("table.user.first.name"));
        }
        if (!Validator.isValid(userLastName.getText())) {
            labelLastName.setText(resourceBundle.getString("table.user.last.name")+" - "+resourceBundle.getString("error.field.empty"));
            labelLastName.setStyle("-fx-text-fill: red");
            valid=false;
        }else {
            labelLastName.setText(resourceBundle.getString("table.user.last.name"));
            labelLastName.setStyle("");
        }
        return valid;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resourceBundle=resources;
        datePicker.setValue(LocalDate.now());
        Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        // Show SATURDAY painted blue
                        DayOfWeek day = DayOfWeek.from(item);
                        if (day == DayOfWeek.SATURDAY) {
                            this.setTextFill(Color.BLUE);
                        }
                        //Show SUNDAY painted red
                        if (day == DayOfWeek.SUNDAY) {
                            this.setTextFill(Color.RED);
                        }
                        //Disable future or outdated day cell
                        if (item.isAfter(LocalDate.now()) || item.isBefore(LocalDate.now().minusYears(5))) {
                            this.setDisable(true);
                        }
                    }
                };
            }
        };
        datePicker.setDayCellFactory(dayCellFactory);
    }
}
