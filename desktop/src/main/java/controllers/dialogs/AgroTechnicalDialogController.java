package controllers.dialogs;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import model.AgroTechnicalTreatmentsRecord;
import model.Validator;
import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * The type Agro technical dialog controller.
 *
 * @author Dawid
 * @version 1
 * @since 2019-05-08
 */
public class AgroTechnicalDialogController implements Initializable {

    ////
    @FXML
    private DatePicker datePicker;
    @FXML
    private TextField species;
    @FXML
    private TextField totalArea;
    @FXML
    private TextField treatmentArea;
    @FXML
    private TextField fertilizerType;
    @FXML
    private TextField fertilizerDose;
    @FXML
    private TextField fertilizerDoseOnArea;
    ////
    @FXML
    private Label labelSpecies;
    @FXML
    private Label labelTotalArea;
    @FXML
    private Label labelTreatmentArea;
    @FXML
    private Label labelFertilizerType;
    @FXML
    private Label labelFertilizerDose;
    @FXML
    private Label labelFertilizerDoseOnArea;

    private ResourceBundle resourceBundle;

    ////
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resourceBundle = resources;
        datePicker.setValue(LocalDate.now());
        Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        // Show SATURDAY painted blue
                        DayOfWeek day = DayOfWeek.from(item);
                        if (day == DayOfWeek.SATURDAY) {
                            this.setTextFill(Color.BLUE);
                        }
                        //Show SUNDAY painted red
                        if (day == DayOfWeek.SUNDAY) {
                            this.setTextFill(Color.RED);
                        }
                        //Disable future or outdated dey cell
                        if (item.isAfter(LocalDate.now())) {
                            this.setDisable(true);
                        }
                    }
                };
            }
        };
        datePicker.setDayCellFactory(dayCellFactory);
        new NumberListener(totalArea);
        new NumberListener(treatmentArea);
        new NumberListener(fertilizerDose);
        new NumberListener(fertilizerDoseOnArea);
    }

    /**
     * Return {@link AgroTechnicalTreatmentsRecord}
     *
     * @return the record
     */
    public AgroTechnicalTreatmentsRecord getRecord() {
        AgroTechnicalTreatmentsRecord record = new AgroTechnicalTreatmentsRecord();
        record.setDate(datePicker.getValue());
        record.setSpecies(species.getText());
        record.setTotalArea(Double.parseDouble(totalArea.getText()));
        record.setTreatmentArea(Double.parseDouble(treatmentArea.getText()));
        record.setFertilizerType(fertilizerType.getText());
        record.setFertilizerDose(Double.parseDouble(fertilizerDose.getText()));
        record.setFertilizerDoseOnArea(Double.parseDouble(fertilizerDoseOnArea.getText()));
        datePicker.setValue(LocalDate.now());
        species.clear();
        totalArea.setText(Double.toString(0.0));
        treatmentArea.setText(Double.toString(0.0));
        fertilizerType.clear();
        fertilizerDose.setText(Double.toString(0.0));
        fertilizerDoseOnArea.setText(Double.toString(0.0));
        return record;
    }

    /**
     * Return true if all field is correct and {@link AgroTechnicalTreatmentsRecord} can be created
     *
     * @return the boolean
     */
    public boolean isValid() {
        boolean valid = true;
        //text validation
        if (!Validator.isValid(species.getText())) {
            labelSpecies.setStyle("-fx-text-fill: red");
            labelSpecies.setText(resourceBundle.getString("dialog.agro.species") + " - " + resourceBundle.getString("error.field.empty"));
            valid = false;
        } else {
            labelSpecies.setStyle("");
            labelSpecies.setText(resourceBundle.getString("dialog.agro.species"));
        }
        if (!Validator.isValid(fertilizerType.getText())) {
            labelFertilizerType.setStyle("-fx-text-fill: red");
            labelFertilizerType.setText(resourceBundle.getString("dialog.agro.fertilizer.type") + " - " + resourceBundle.getString("error.field.empty"));
            valid = false;
        } else {
            labelFertilizerType.setStyle("");
            labelFertilizerType.setText(resourceBundle.getString("dialog.agro.fertilizer.type"));
        }

        //numeric validation
        if (!Validator.isValid(Double.parseDouble(totalArea.getText()))) {
            labelTotalArea.setStyle("-fx-text-fill: red");
            labelTotalArea.setText(resourceBundle.getString("dialog.agro.area") + " - " + resourceBundle.getString("error.field.incorrect"));
            valid = false;
        } else {
            labelTotalArea.setStyle("");
            labelTotalArea.setText(resourceBundle.getString("dialog.agro.area"));
        }
        if (!Validator.isValid(Double.parseDouble(treatmentArea.getText()), null, Double.parseDouble(totalArea.getText()))) {
            labelTreatmentArea.setStyle("-fx-text-fill: red");
            labelTreatmentArea.setText(resourceBundle.getString("dialog.agro.area.treatment") + " - " + resourceBundle.getString("error.field.incorrect"));
            valid = false;
        } else {
            labelTreatmentArea.setStyle("");
            labelTreatmentArea.setText(resourceBundle.getString("dialog.agro.area.treatment"));
        }
        if (!Validator.isValid(Double.parseDouble(fertilizerDose.getText()))) {
            labelFertilizerDose.setStyle("-fx-text-fill: red");
            labelFertilizerDose.setText(resourceBundle.getString("dialog.agro.dose") + " - " + resourceBundle.getString("error.field.incorrect"));
            valid = false;
        } else {
            labelFertilizerDose.setStyle("");
            labelFertilizerDose.setText(resourceBundle.getString("dialog.agro.dose"));
        }
        if (!Validator.isValid(Double.parseDouble(fertilizerDoseOnArea.getText()))) {
            labelFertilizerDoseOnArea.setStyle("-fx-text-fill: red");
            labelFertilizerDoseOnArea.setText(resourceBundle.getString("dialog.agro.dose.on.area") + " - " + resourceBundle.getString("error.field.out.of.range"));
            valid = false;
        } else {
            labelFertilizerDoseOnArea.setStyle("");
            labelFertilizerDoseOnArea.setText(resourceBundle.getString("dialog.agro.dose.on.area"));
        }
        return valid;
    }

}
