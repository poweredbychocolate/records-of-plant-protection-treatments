package controllers.dialogs;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import javafx.util.StringConverter;
import model.PlantProtectionRecord;
import model.User;
import model.Validator;
import services.DatabaseService;

import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * The type User dialog controller. Get entered data from new user form an create user instance.
 *
 * @author Dawid
 * @version 2
 * @since 2019 -04-05
 */
public class PlantsDialogController implements Initializable {
    private DatabaseService databaseService;
    ////
    @FXML
    private DatePicker datePicker;
    @FXML
    private TextField plant;
    @FXML
    private TextField totalArea;
    @FXML
    private TextField treatmentArea;
    @FXML
    private TextField areaName;
    @FXML
    private TextField protectionName;
    @FXML
    private ListView<String> protectionNameList;
    @FXML
    private TextField protectionDose;
    @FXML
    private TextField liquidDose;
    @FXML
    private TextField reasonUse;
    @FXML
    private TextArea comments;
    @FXML
    private ChoiceBox<User> user;
    @FXML
    private Label labelPlant;
    @FXML
    private Label labelTotalArea;
    @FXML
    private Label labelTreatmentArea;
    @FXML
    private Label labelAreaName;
    @FXML
    private Label labelProtectionName;
    @FXML
    private Label labelProtectionDose;
    @FXML
    private Label labelLiquidDose;
    @FXML
    private Label labelReasonUse;
    @FXML
    private Label labelComments;

    private ResourceBundle resourceBundle;

    ////
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resourceBundle = resources;
        databaseService = DatabaseService.getDB();
        datePicker.setValue(LocalDate.now());
        Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        // Show SATURDAY painted blue
                        DayOfWeek day = DayOfWeek.from(item);
                        if (day == DayOfWeek.SATURDAY) {
                            this.setTextFill(Color.BLUE);
                        }
                        //Show SUNDAY painted red
                        if (day == DayOfWeek.SUNDAY) {
                            this.setTextFill(Color.RED);
                        }
                        //Disable future or outdated dey cell
                        if (item.isAfter(LocalDate.now())) {
                            this.setDisable(true);
                        }
                    }
                };
            }
        };
        datePicker.setDayCellFactory(dayCellFactory);

        new NumberListener(totalArea);
        new NumberListener(treatmentArea);
        new NumberListener(protectionDose);
        new NumberListener(liquidDose);

        user.setItems(FXCollections.observableList(databaseService.findAll(User.class)));
        user.getSelectionModel().selectFirst();
        user.setConverter(new StringConverter<User>() {
            @Override
            public String toString(User object) {
                if (object != null) return object.getFirstName() + " " + object.getLastName();
                return null;
            }

            @Override
            public User fromString(String string) {
                if (string != null) {
                    String firstN = string.substring(0, string.indexOf(" "));
                    String lastN = string.substring(string.indexOf(" ") + 1);
                    return user.getItems().stream().filter(u -> u.getFirstName().equals(firstN) && u.getLastName().equals(lastN)).findFirst().orElse(null);
                }
                return null;
            }
        });
        protectionName.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue!=null&&!newValue.isEmpty()) {
                ObservableList<String> observableList = FXCollections.observableArrayList();
                observableList.add(newValue);
                observableList.addAll(databaseService.findProductNames(newValue));
                protectionNameList.setItems(observableList);
                protectionNameList.getSelectionModel().selectFirst();
            } else {
                protectionNameList.setItems(null);
            }
        });


    }

    /**
     * Crete new {@link User} using form data
     *
     * @return the user
     */
    public PlantProtectionRecord getRecord() {
        PlantProtectionRecord record = new PlantProtectionRecord();
        record.setUser(user.getValue());
        record.setDate(datePicker.getValue());
        record.setPlant(plant.getText());
        record.setTotalArea(Double.parseDouble(totalArea.getText()));
        record.setTreatmentArea(Double.parseDouble(treatmentArea.getText()));
        record.setAreaName(areaName.getText());
        record.setProtectionName(protectionNameList.getSelectionModel().getSelectedItem());
        record.setProtectionDose(Double.parseDouble(protectionDose.getText()));
        record.setLiquidDose(Double.parseDouble(liquidDose.getText()));
        record.setReasonUse(reasonUse.getText());
        record.setComments(comments.getText());
        datePicker.setValue(LocalDate.now());
        plant.clear();
        totalArea.setText(Double.toString(0.0));
        treatmentArea.setText(Double.toString(0.0));
        areaName.clear();
        protectionName.clear();
        protectionNameList.setItems(null);
        protectionDose.setText(Double.toString(0.0));
        liquidDose.setText(Double.toString(0.0));
        reasonUse.clear();
        comments.clear();
        user.getSelectionModel().selectFirst();
        return record;
    }

    /**
     * Return true if all form data is correct
     *
     * @return the boolean
     */
    public boolean isValid() {
        boolean valid = true;
        //text validation
        if (!Validator.isValid(plant.getText())) {
            labelPlant.setStyle("-fx-text-fill: red");
            labelPlant.setText(resourceBundle.getString("table.plant.name") + " - " + resourceBundle.getString("error.field.empty"));
            valid = false;
        } else {
            labelPlant.setStyle("");
            labelPlant.setText(resourceBundle.getString("table.plant.name"));
        }
        if (!Validator.isValid(areaName.getText())) {
            labelAreaName.setStyle("-fx-text-fill: red");
            labelAreaName.setText(resourceBundle.getString("table.plant.area.name") + " - " + resourceBundle.getString("error.field.empty"));
            valid = false;
        } else {
            labelAreaName.setStyle("");
            labelAreaName.setText(resourceBundle.getString("table.plant.area.name"));
        }
        if (!Validator.isValid(protectionName.getText())) {
            labelProtectionName.setStyle("-fx-text-fill: red");
            labelProtectionName.setText(resourceBundle.getString("dialog.plant.protection.name") + " - " + resourceBundle.getString("error.field.empty"));
            valid = false;
        } else {
            labelProtectionName.setStyle("");
            labelProtectionName.setText(resourceBundle.getString("dialog.plant.protection.name"));

        }
        if (!Validator.isValid(reasonUse.getText())) {
            labelReasonUse.setStyle("-fx-text-fill: red");
            labelReasonUse.setText(resourceBundle.getString("dialog.plant.reason.use") + " - " + resourceBundle.getString("error.field.empty"));
            valid = false;
        } else {
            labelReasonUse.setStyle("");
            labelReasonUse.setText(resourceBundle.getString("dialog.plant.reason.use"));
        }
        if (!Validator.isValid(comments.getText())) {
            labelComments.setStyle("-fx-text-fill: red");
            labelComments.setText(resourceBundle.getString("table.plant.comments") + " - " + resourceBundle.getString("error.field.empty"));
            valid = false;
        } else {
            labelComments.setStyle("");
            labelComments.setText(resourceBundle.getString("table.plant.comments"));
        }
        //numeric validation
        if (!Validator.isValid(Double.parseDouble(totalArea.getText()))) {
            labelTotalArea.setStyle("-fx-text-fill: red");
            labelTotalArea.setText(resourceBundle.getString("table.plant.area.total") + " - " + resourceBundle.getString("error.field.incorrect"));
            valid = false;
        } else {
            labelTotalArea.setStyle("");
            labelTotalArea.setText(resourceBundle.getString("table.plant.area.total"));
        }
        if (!Validator.isValid(Double.parseDouble(protectionDose.getText()))) {
            labelProtectionDose.setStyle("-fx-text-fill: red");
            labelProtectionDose.setText(resourceBundle.getString("dialog.plant.protection.dose") + " - " + resourceBundle.getString("error.field.incorrect"));
            valid = false;
        } else {
            labelProtectionDose.setStyle("");
            labelProtectionDose.setText(resourceBundle.getString("dialog.plant.protection.dose"));
        }
        if (!Validator.isValid(Double.parseDouble(liquidDose.getText()))) {
            labelLiquidDose.setStyle("-fx-text-fill: red");
            labelLiquidDose.setText(resourceBundle.getString("table.plant.liquid") + " - " + resourceBundle.getString("error.field.incorrect"));
            valid = false;
        } else {
            labelLiquidDose.setStyle("");
            labelLiquidDose.setText(resourceBundle.getString("table.plant.liquid"));
        }
        if (!Validator.isValid(Double.parseDouble(treatmentArea.getText()), null, Double.parseDouble(totalArea.getText()))) {
            labelTreatmentArea.setStyle("-fx-text-fill: red");
            labelTreatmentArea.setText(resourceBundle.getString("dialog.plant.area.treatment") + " - " + resourceBundle.getString("error.field.out.of.range"));
            valid = false;
        } else {
            labelTreatmentArea.setStyle("");
            labelTreatmentArea.setText(resourceBundle.getString("dialog.plant.area.treatment"));
        }
        return valid;
    }

}
