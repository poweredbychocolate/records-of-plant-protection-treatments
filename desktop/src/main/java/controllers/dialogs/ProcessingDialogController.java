package controllers.dialogs;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class ProcessingDialogController implements Initializable {
    private String d = "...";
    private boolean work = true;
    @FXML
    private Label dots;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void start() {
        Platform.runLater(() -> {
            while (work) {
                dots.setText(d);
                d += ".";
                if (d.length() > 6) d = "...";
            }
        });
    }

    public void stop() {
        work = false;
    }
}
