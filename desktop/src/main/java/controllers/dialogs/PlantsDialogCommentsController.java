package controllers.dialogs;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

/**
 * The type Plants dialog comments controller.
 */
public class PlantsDialogCommentsController {
    @FXML
    private TextArea info;

    /**
     * Display comments.
     *
     * @param text the text
     */
    public void displayComments(String text){
        info.setText(text);
    }
}
