package model;

/**
 * The interface Entity id.
 * @author Dawid
 * @version 1
 *
 */
public interface EntityId {
    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getId();

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Integer id);
}
