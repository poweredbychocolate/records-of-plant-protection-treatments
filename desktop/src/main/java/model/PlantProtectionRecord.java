package model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * The type Plant protection record.
 *
 * @author Dawid
 * @version 1
 * @since 2019 -04-07
 */
@Entity
public class PlantProtectionRecord implements Serializable,EntityId {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(nullable = false)
    private LocalDate date;
    // plant name
    @Column(nullable = false)
    private String plant;
    // total crop area
    @Column(nullable = false)
    private Double totalArea;
    // area of the field where the treatment was performed
    @Column(nullable = false)
    private Double treatmentArea;
    // area name or number
    @Column(nullable = false)
    private String areaName;
    // name of the plant protection product
    @Column(nullable = false)
    private String protectionName;
    // the dose of the plant protection product liter or kg or %
    @Column(nullable = false)
    private Double protectionDose;
    // the dose of liquid
    @Column(nullable = false)
    private Double liquidDose;
    // the reason for using a plant protection product
    @Column(nullable = false)
    private String reasonUse;
    @Column(nullable = false)
    @Lob
    private String comments;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userFK",nullable = false)
    private User user;

    /**
     * Instantiates a new Plant protection record.
     */
    public PlantProtectionRecord(){
        id=0;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * Gets plant.
     *
     * @return the plant
     */
    public String getPlant() {
        return plant;
    }

    /**
     * Sets plant.
     *
     * @param plant the plant
     */
    public void setPlant(String plant) {
        this.plant = plant;
    }

    /**
     * Gets total area.
     *
     * @return the total area
     */
    public Double getTotalArea() {
        return totalArea;
    }

    /**
     * Sets total area.
     *
     * @param totalArea the total area
     */
    public void setTotalArea(Double totalArea) {
        this.totalArea = totalArea;
    }

    /**
     * Gets treatment area.
     *
     * @return the treatment area
     */
    public Double getTreatmentArea() {
        return treatmentArea;
    }

    /**
     * Sets treatment area.
     *
     * @param treatmentArea the treatment area
     */
    public void setTreatmentArea(Double treatmentArea) {
        this.treatmentArea = treatmentArea;
    }

    /**
     * Gets area name.
     *
     * @return the area name
     */
    public String getAreaName() {
        return areaName;
    }

    /**
     * Sets area name.
     *
     * @param areaName the area name
     */
    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    /**
     * Gets protection name.
     *
     * @return the protection name
     */
    public String getProtectionName() {
        return protectionName;
    }

    /**
     * Sets protection name.
     *
     * @param protectionName the protection name
     */
    public void setProtectionName(String protectionName) {
        this.protectionName = protectionName;
    }

    /**
     * Gets protection dose.
     *
     * @return the protection dose
     */
    public Double getProtectionDose() {
        return protectionDose;
    }

    /**
     * Sets protection dose.
     *
     * @param protectionDose the protection dose
     */
    public void setProtectionDose(Double protectionDose) {
        this.protectionDose = protectionDose;
    }

    /**
     * Gets liquid dose.
     *
     * @return the liquid dose
     */
    public Double getLiquidDose() {
        return liquidDose;
    }

    /**
     * Sets liquid dose.
     *
     * @param liquidDose the liquid dose
     */
    public void setLiquidDose(Double liquidDose) {
        this.liquidDose = liquidDose;
    }

    /**
     * Gets reason use.
     *
     * @return the reason use
     */
    public String getReasonUse() {
        return reasonUse;
    }

    /**
     * Sets reason use.
     *
     * @param reasonUse the reason use
     */
    public void setReasonUse(String reasonUse) {
        this.reasonUse = reasonUse;
    }

    /**
     * Gets comments.
     *
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets comments.
     *
     * @param comments the comments
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * Gets user.
     *
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * Sets user.
     *
     * @param user the user
     */
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "PlantProtectionRecord{" +
                "id=" + id +
                ", date=" + date +
                ", plant='" + plant + '\'' +
                ", totalArea=" + totalArea +
                ", treatmentArea=" + treatmentArea +
                ", fieldName='" + areaName + '\'' +
                ", protectionName='" + protectionName + '\'' +
                ", protectionDose=" + protectionDose +
                ", liquidDose=" + liquidDose +
                ", reasonUse='" + reasonUse + '\'' +
                ", comments='" + comments + '\'' +
                ", user=" + user +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlantProtectionRecord record = (PlantProtectionRecord) o;
        return Objects.equals(id, record.id) &&
                Objects.equals(date, record.date) &&
                Objects.equals(plant, record.plant) &&
                Objects.equals(totalArea, record.totalArea) &&
                Objects.equals(treatmentArea, record.treatmentArea) &&
                Objects.equals(areaName, record.areaName) &&
                Objects.equals(protectionName, record.protectionName) &&
                Objects.equals(protectionDose, record.protectionDose) &&
                Objects.equals(liquidDose, record.liquidDose) &&
                Objects.equals(reasonUse, record.reasonUse) &&
                Objects.equals(comments, record.comments) &&
                Objects.equals(user, record.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, plant, totalArea, treatmentArea, areaName, protectionName, protectionDose, liquidDose, reasonUse, comments, user);
    }
}
