package model;

import java.time.LocalDate;
import java.time.Period;

/**
 * The type Validator.
 */
public class Validator {
    /**
     * The constant PLANT_PROTECTION_VALIDITY_YEARS.
     */
    public static final int PLANT_PROTECTION_VALIDITY_YEARS = 5;

    /**
     * Check if {@link User} has valid certificate .
     *
     * @param user the user
     * @return the boolean
     */
    public static boolean hasValidCertificate(User user) {
        if (Period.between(user.getObtainingDate(), LocalDate.now()).getYears() >= PLANT_PROTECTION_VALIDITY_YEARS) {
            return false;
        }
        return true;
    }

    /**
     * Check if form text field is valid.
     *
     * @param string the string
     * @return the boolean
     */
    public static boolean isValid(String string) {
        if (string != null && string.length() > 0) return true;
        return false;
    }

    /**
     * Check if form double field is valid.
     *
     * @param value the value
     * @return the boolean
     */
    public static boolean isValid(Double value) {
        if (value != null && value >= 0.0)
            return true;
        return false;
    }

    /**
     * Check if form double field is between minValue and maxValue.
     *
     * @param value    the value
     * @param minValue the min value
     * @param maxValue the max value
     * @return the boolean
     */
    public static boolean isValid(Double value, Double minValue,Double maxValue) {
        if (value != null &&minValue!=null&&maxValue==null && value >= minValue)
            return true;
        if (value != null &&maxValue!=null&&minValue==null && value >= 0.0 && value <= maxValue)
            return true;
        if (value != null &&maxValue!=null&&minValue!=null && value >= minValue && value <= maxValue)
            return true;
        return false;
    }
}
