package app;

import javafx.application.HostServices;
import javafx.scene.control.Hyperlink;

public class PageLoader {
    private static HostServices hostServices;
    private static PageLoader pageLoader;
    private PageLoader(HostServices hostServices){
        PageLoader.hostServices =hostServices;
    }
    public static void open(Hyperlink hyperlink){
        if(hostServices!=null) hostServices.showDocument(hyperlink.getText());
    }
    public static void init(HostServices hostServices){
        if(pageLoader==null) pageLoader=new PageLoader(hostServices);
    }
}
