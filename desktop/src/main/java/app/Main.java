package app;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import services.DatabaseService;
import services.DiscordLogService;
import java.util.ResourceBundle;

/**
 * The type Main.
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Platform.runLater(() -> DatabaseService.getDB());
        //DatabaseService.getDB();
        PageLoader.init(getHostServices());
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/main_window.fxml"));
        fxmlLoader.setResources(ResourceBundle.getBundle("lang.file"));
        Parent root = fxmlLoader.load();
        new Thread(()->DiscordLogService.get().openChannel("NTgyMzIwOTExMDg5MjcwODUw.XOsJ2g.P35bky6CoUQzOrRvllYGy-TkeDU", "582332623104376851")).start();
        primaryStage.setTitle("Katalog ewidencji");
        primaryStage.setMinWidth(1024.0);
        primaryStage.setMinHeight(600.0);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
