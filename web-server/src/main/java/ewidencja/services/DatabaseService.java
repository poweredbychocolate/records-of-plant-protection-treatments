package ewidencja.services;

import ewidencja.model.AgriculturalSale;
import ewidencja.model.EntityId;
import ewidencja.model.PlantProtectionProduct;
import ewidencja.model.User;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Repository
public class DatabaseService implements AutoCloseable {
    @PersistenceContext
    private EntityManager entityManager;
    private SessionFactory sessionFactory;
    @PostConstruct
    private void init() {
        sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
    }

    @Override
    public void close() {
        sessionFactory.close();
    }

    public synchronized <T> Optional<T> save(T toSave) {
        Session session = null;
        Transaction transaction = null;
        Optional<T> tmp;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            tmp = Optional.ofNullable((T) session.merge(toSave));
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            tmp = Optional.empty();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return tmp;
    }

    public synchronized <T extends EntityId> boolean remove(Class<T> type, T toRemove) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            T tmp = session.find(type, toRemove.getId());
            if (tmp != null) {
                session.remove(tmp);
                transaction.commit();
                return true;
            }
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return false;
    }

    public <T> Optional<List<T>> findAll(Class<T> type) {
        Optional<List<T>> list;
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<T> criteriaQuery = builder.createQuery(type);
            Root<T> root = criteriaQuery.from(type);
            criteriaQuery.select(root);
            list = Optional.ofNullable(session.createQuery(criteriaQuery).getResultList());
        } catch (Exception e) {
            list = Optional.empty();
        }
        return list;
    }

    public <T> Optional<T> findById(Class<T> type, Integer id) {
        Optional<T> object;
        try (Session session = sessionFactory.openSession()) {
            object = Optional.ofNullable(session.find(type, id));
        } catch (Exception e) {
            object = Optional.empty();
        }
        return object;
    }

    public Optional<List<User>> findExpiringCertificates(LocalDate date) {
        Optional<List<User>> list;
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<User> criteriaQuery = builder.createQuery(User.class);
            Root<User> root = criteriaQuery.from(User.class);
            criteriaQuery.select(root);
            criteriaQuery.where(builder.lessThanOrEqualTo(root.get("obtainingDate"), date));
            list = Optional.ofNullable(session.createQuery(criteriaQuery).getResultList());
        } catch (Exception e) {
            list = Optional.empty();
        }
        return list;
    }

    public synchronized <T> boolean save(List<T> toSave) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            int counter = 0;
            for (T tmp : toSave) {
                session.merge(tmp);
                counter++;
                if (counter >= 100) {
                    counter = 0;
                    session.flush();
                    session.clear();
                }
            }
            transaction.commit();
            return true;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            return false;
        }
    }

    public Optional<List<PlantProtectionProduct>> findAllProduct() {
        Optional<List<PlantProtectionProduct>> list;
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<PlantProtectionProduct> criteriaQuery = builder.createQuery(PlantProtectionProduct.class);
            Root<PlantProtectionProduct> root = criteriaQuery.from(PlantProtectionProduct.class);
            criteriaQuery.select(root);
            criteriaQuery.orderBy(builder.asc(root.get("name")));
            list = Optional.ofNullable(session.createQuery(criteriaQuery).getResultList());
        } catch (Exception e) {
            list = Optional.empty();
        }
        return list;
    }

    public Optional<List<String>> findProductTypes() {
        Optional<List<String>> list;
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<String> criteriaQuery = builder.createQuery(String.class);
            Root<PlantProtectionProduct> root = criteriaQuery.from(PlantProtectionProduct.class);
            criteriaQuery.select(root.get("type")).distinct(true);
            criteriaQuery.orderBy(builder.asc(root.get("type")));
            list = Optional.ofNullable(session.createQuery(criteriaQuery).getResultList());
        } catch (Exception e) {
            list = Optional.empty();
        }
        return list;
    }

    public Optional<List<PlantProtectionProduct>> findAllProduct(String name, String type) {
        Optional<List<PlantProtectionProduct>> list;
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<PlantProtectionProduct> criteriaQuery = builder.createQuery(PlantProtectionProduct.class);
            Root<PlantProtectionProduct> root = criteriaQuery.from(PlantProtectionProduct.class);
            criteriaQuery.select(root);
            List<Predicate> predicates = new ArrayList<>();
            if (name != null && !name.equals("")) {
                Predicate predicate = builder.like(builder.lower(root.get("name")), name.toLowerCase() + "%");
                predicates.add(predicate);
            }
            if (type != null) {
                Predicate predicate = builder.like(root.get("type"), type.equals("") ? type : "%" + type + "%");
                predicates.add(predicate);
            }
            if (predicates.size() == 1) {
                criteriaQuery.where(predicates.get(0));
            }
            if (predicates.size() > 1) {
                criteriaQuery.where(builder.and(predicates.toArray(new Predicate[]{})));
            }

            criteriaQuery.orderBy(builder.asc(root.get("name")));
            list = Optional.ofNullable(session.createQuery(criteriaQuery).getResultList());
        } catch (Exception e) {
            list = Optional.empty();
        }
        return list;
    }

    public Optional<List<String>> findProductNames(String pattern) {
        Optional<List<String>> list;
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<String> criteriaQuery = builder.createQuery(String.class);
            Root<PlantProtectionProduct> root = criteriaQuery.from(PlantProtectionProduct.class);
            criteriaQuery.select(root.get("name")).distinct(true);
            criteriaQuery.orderBy(builder.asc(root.get("name")));
            criteriaQuery.where(builder.like(builder.lower(root.get("name")), pattern.toLowerCase() + "%"));
            list = Optional.ofNullable(session.createQuery(criteriaQuery).getResultList());
        } catch (Exception e) {
            list = Optional.empty();
        }
        return list;
    }

    public Optional<List<String>> findSaleNames() {
        Optional<List<String>> list;
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<String> criteriaQuery = builder.createQuery(String.class);
            Root<AgriculturalSale> root = criteriaQuery.from(AgriculturalSale.class);
            criteriaQuery.select(root.get("name")).distinct(true);
            criteriaQuery.orderBy(builder.asc(root.get("name")));
            list = Optional.ofNullable(session.createQuery(criteriaQuery).getResultList());
        }catch (Exception e) {
            list = Optional.empty();
        }
        return list;
    }

    public Optional<List<String>> findSaleDates() {
        Optional<List<String>> list = null;
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<LocalDate> criteriaQuery = builder.createQuery(LocalDate.class);
            Root<AgriculturalSale> root = criteriaQuery.from(AgriculturalSale.class);
            criteriaQuery.select(root.get("date"));
            criteriaQuery.orderBy(builder.asc(root.get("date")));
            List<Integer> tmp = session.createQuery(criteriaQuery).getResultList().stream().map(LocalDate::getYear).distinct().collect(Collectors.toList());
            list = Optional.of(tmp.stream().map(Object::toString).collect(Collectors.toList()));
        } catch (Exception e) {
            list = Optional.empty();
        }
        return list;
    }

    public Optional<List<AgriculturalSale>> findAllSale(String date, String name) {
        Optional<List<AgriculturalSale>> list = null;
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<AgriculturalSale> criteriaQuery = builder.createQuery(AgriculturalSale.class);
            Root<AgriculturalSale> root = criteriaQuery.from(AgriculturalSale.class);
            criteriaQuery.select(root);
            List<Predicate> predicates = new ArrayList<>();
            if (date != null) {
//                Predicate predicate = builder.like(root.get("date"), "%"+ DateTimeFormatter.ofPattern("yyyy").parse(date) + "%");
                Predicate predicate = builder.and(builder.lessThanOrEqualTo(root.get("date"), LocalDate.of(Integer.parseInt(date), Month.DECEMBER, 31)),
                        builder.greaterThanOrEqualTo(root.get("date"), LocalDate.of(Integer.parseInt(date), Month.JANUARY, 1)));
                predicates.add(predicate);
            }
            if (name != null) {
                Predicate predicate = builder.like(root.get("name"), name + "%");
                predicates.add(predicate);
            }
            if (predicates.size() == 1) {
                criteriaQuery.where(predicates.get(0));
            }
            if (predicates.size() > 1) {
                criteriaQuery.where(builder.and(predicates.toArray(new Predicate[]{})));
            }
            criteriaQuery.orderBy(builder.asc(root.get("date")), builder.asc(root.get("name")));
            list = Optional.ofNullable(session.createQuery(criteriaQuery).getResultList());
        } catch (Exception e) {
            list = Optional.empty();
        }
        return list;
    }

    public Optional<List<String>> findSalesNames(String pattern) {
        Optional<List<String>> list = null;
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<String> criteriaQuery = builder.createQuery(String.class);
            Root<AgriculturalSale> root = criteriaQuery.from(AgriculturalSale.class);
            criteriaQuery.select(root.get("name")).distinct(true);
            criteriaQuery.orderBy(builder.asc(root.get("name")));
            criteriaQuery.where(builder.like(builder.lower(root.get("name")), pattern.toLowerCase() + "%"));
            list = Optional.ofNullable(session.createQuery(criteriaQuery).getResultList());
        } catch (Exception e) {
            list = Optional.empty();
        }
        return list;
    }

}
