package ewidencja.services;

import discord4j.core.DiscordClient;
import discord4j.core.DiscordClientBuilder;
import discord4j.core.object.entity.TextChannel;
import discord4j.core.object.util.Snowflake;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
@Service
public class DiscordLogService {
    private DiscordClient discordClient;
    private TextChannel textChannel;
    private static DiscordLogService SERVICE;

    DiscordLogService() {
    }

    public synchronized DiscordLogService openChannel(String botToken, String channelId) {
        try {
            discordClient = new DiscordClientBuilder(botToken).build();
            textChannel = (TextChannel) discordClient.getChannelById(Snowflake.of(channelId)).block(Duration.ofMinutes(1));
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Discord Logger is dead");
        }
        return SERVICE;
    }

    public boolean write(String title, LocalDateTime dateTime, String description) {
        if (textChannel != null) {
            try {
                textChannel.createMessage("```" + System.lineSeparator() + "```").subscribe();
                textChannel.createMessage("__**" + title + "**__").subscribe();
                textChannel.createMessage("__" + dateTime.toString() + "__").subscribe();
                textChannel.createMessage("*" + description + "*").subscribe();
                return true;
            } catch (Exception e) {
                System.err.println("Cannot write message");
            }
        }
        return false;
    }

    public synchronized boolean write(String title, LocalDateTime dateTime, Exception exception) {
        if (textChannel != null) {
            try {
                textChannel.createMessage("```" + System.lineSeparator() + title + System.lineSeparator()+ dateTime.toString() +System.lineSeparator()+"```").subscribe();
                textChannel.createMessage("__**" + title + "**__").subscribe();
                textChannel.createMessage("__" + dateTime.toString() + "__").subscribe();

                StringBuilder stringBuilder = new StringBuilder();
                for (StackTraceElement element : exception.getStackTrace()) {
                    if (stringBuilder.length() + element.toString().length() + 4 >= 2000) {
                        textChannel.createMessage(stringBuilder.toString()).subscribe();
                        stringBuilder.delete(0, stringBuilder.length());
                    }
                    stringBuilder.append("*");
                    stringBuilder.append(element);
                    stringBuilder.append("*");
                    stringBuilder.append(System.lineSeparator());
                }
                textChannel.createMessage(stringBuilder.toString()).subscribe();
                return true;
            } catch (Exception e) {
                System.err.println("Cannot write exception");
            }
        }
        return false;
    }

}
