package ewidencja;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class EEwidencjaWebApplication {
	public static void main(String[] args) {
		SpringApplication.run(EEwidencjaWebApplication.class, args);
	}
}
