package ewidencja.rest.controllers;

import ewidencja.model.AgriculturalSale;
import ewidencja.model.AgroTechnicalTreatmentsRecord;
import ewidencja.model.PlantProtectionProduct;
import ewidencja.services.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/agricultural")
public class AgriculturalSaleController extends GodlikeController<AgriculturalSale> {
    @Autowired
    private DatabaseService databaseService;

    public AgriculturalSaleController() {
        super(AgriculturalSale.class);
    }
    @GetMapping("/names")
    Object findNames() {
        Optional<List<String>> list = databaseService.findSaleNames();
        if (list.isPresent()) {
            return list.get();
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping("/dates")
    Object findDates() {
        Optional<List<String>> list = databaseService.findSaleDates();
        if (list.isPresent()) {
            return list.get();
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/filter/{date}/{name}")
    Object filter(@PathVariable("name") String name, @PathVariable("date") String date) {
        if(name.equalsIgnoreCase("null")) name=null;
        if(date.equalsIgnoreCase("null")) date=null;
        Optional<List<AgriculturalSale>> list = databaseService.findAllSale(date,name);
        if (list.isPresent()) {
            return list.get();
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping("/names/{pattern}")
    Object findNames(@PathVariable("pattern") String pattern) {
        Optional<List<String>> list = databaseService.findSalesNames(pattern);
        if (list.isPresent()&&!list.get().isEmpty()) {
            return list.get();
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

}
