package ewidencja.rest.controllers;

import ewidencja.model.PlantProtectionRecord;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/plant/protection/record")
public class PlantProtectionRecordController extends GodlikeController<PlantProtectionRecord> {
    public PlantProtectionRecordController() {
        super(PlantProtectionRecord.class);
    }
}
