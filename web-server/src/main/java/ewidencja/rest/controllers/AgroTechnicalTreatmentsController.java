package ewidencja.rest.controllers;

import ewidencja.model.AgroTechnicalTreatmentsRecord;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/agro")
public class AgroTechnicalTreatmentsController extends GodlikeController<AgroTechnicalTreatmentsRecord> {

    public AgroTechnicalTreatmentsController() {
        super(AgroTechnicalTreatmentsRecord.class);
    }
}
