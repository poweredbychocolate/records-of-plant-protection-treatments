package ewidencja.rest.controllers;

import ewidencja.model.EntityId;
import ewidencja.services.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;

public abstract class GodlikeController<T extends EntityId> {
    @Autowired
    private DatabaseService databaseService;
    private Class<T> type;


    public GodlikeController(Class<T> type) {
        this.type = type;
    }

    @GetMapping
    Object getAll() {
        Optional optional = databaseService.findAll(type);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    Object save(@RequestBody T toSave) {
        try {
            Optional<T> tmp = databaseService.save(toSave);
            if (tmp.isPresent()) {
                return tmp.get();
            } else {
                return new ResponseEntity(HttpStatus.CONFLICT);
            }
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}")
    Object remove(@PathVariable("id") Integer id) {
        try {
            Optional<T> tmp = databaseService.findById(type, id);
            if (tmp.isPresent()) {
                boolean status = databaseService.remove(type, tmp.get());
                if (status) return new ResponseEntity(HttpStatus.OK);
            }
            return new ResponseEntity(HttpStatus.NOT_FOUND);

        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/remove")
    Object remove(@RequestBody T toRemove) {
        try {
            boolean status = databaseService.remove(type, toRemove);
            return status ? toRemove : new ResponseEntity(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/find/{id}")
    Object findById(@PathVariable("id") Integer id) {
        try {
            Optional<T> tmp = databaseService.findById(type, id);
            if (tmp.isPresent()) {
                return tmp.get();
            } else {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
