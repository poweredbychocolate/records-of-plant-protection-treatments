package ewidencja.rest.controllers;

import ewidencja.model.User;
import ewidencja.services.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/user")
public class UserController extends GodlikeController<User> {
    @Autowired
    private DatabaseService databaseService;

    public UserController() {
        super(User.class);
    }

    @Override
    Object save(@RequestBody User user) {
        try {
            user.setType(User.TYPE_PLANT_PROTECTION);
            Optional<User> tmp = databaseService.save(user);
            if (tmp.isPresent()) {
                return tmp.get();
            } else {
                return new ResponseEntity(HttpStatus.CONFLICT);
            }
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping("/expiring/certificates/{date}")
    Object expiringCertificates(@PathVariable("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        try {
            Optional<List<User>> list = databaseService.findExpiringCertificates(date);
            if(list.isPresent()){
                return list.get();
            }else {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
