package ewidencja.rest.controllers;

import ewidencja.model.PlantProtectionProduct;
import ewidencja.services.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/plant/protection/product")
public class PlantProtectionProductController extends GodlikeController<PlantProtectionProduct> {
    @Autowired
    private DatabaseService databaseService;

    public PlantProtectionProductController() {
        super(PlantProtectionProduct.class);
    }

    @GetMapping("/sorted")
    Object findAll() {
        Optional<List<PlantProtectionProduct>> list = databaseService.findAllProduct();
        if (list.isPresent()) {
            return list.get();
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping("/types")
    Object findTypes() {
        Optional<List<String>> list = databaseService.findProductTypes();
        if (list.isPresent()) {
            return list.get();
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping("/filter/{name}/{type}")
    Object filter(@PathVariable("name") String name, @PathVariable("type") String type) {
        if(name.equalsIgnoreCase("*")) name=null;
        if(type.equalsIgnoreCase("*")) type=null;
        Optional<List<PlantProtectionProduct>> list = databaseService.findAllProduct(name,type);
        if (list.isPresent()) {
            return list.get();
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping("/names/{pattern}")
    Object findNames(@PathVariable("pattern") String pattern) {
        Optional<List<String>> list = databaseService.findProductNames(pattern);
        if (list.isPresent()) {
            return list.get();
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }  @GetMapping("/names")
    Object findNames() {
        Optional<List<String>> list = databaseService.findProductNames("");
        if (list.isPresent()) {
            return list.get();
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
