package ewidencja.model;


import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * The type User.
 * @author Dawid
 * @since 2019-03-31
 * @version 2
 */
@Entity
public class User implements Serializable,EntityId {
    /**
     * The constant TYPE_PLANT_PROTECTION.
     */
    public static final String TYPE_PLANT_PROTECTION="PLANT_PROTECTION";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(nullable = false)
    private String type;
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    @Column(nullable = false)
    private LocalDate obtainingDate;

    /**
     * Instantiates a new User.
     */
    public User() {
        id=0;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     * @return the type
     */
    public boolean setType(String type) {
        if(type.equals(TYPE_PLANT_PROTECTION)) {
            this.type = type;
            return true;
        }
        return false;
    }

    /**
     * Gets first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name.
     *
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets last name.
     *
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets last name.
     *
     * @param lastName the last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    /**
     * Gets obtaining date.
     *
     * @return the obtaining date
     */
    public LocalDate getObtainingDate() {
        return obtainingDate;
    }

    /**
     * Sets obtaining date.
     *
     * @param obtainingDate the obtaining date
     */
    public void setObtainingDate(LocalDate obtainingDate) {
        this.obtainingDate = obtainingDate;
    }



    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", obtainingDate=" + obtainingDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return  Objects.equals(id, user.id) &&
                Objects.equals(type, user.type) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(obtainingDate, user.obtainingDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, firstName, lastName, obtainingDate);
    }
}
