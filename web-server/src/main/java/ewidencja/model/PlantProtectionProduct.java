package ewidencja.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

/**
 * The type Plant protection product.
 *
 * @author Dawid
 * @version 1
 * @since 2019-05-09
 */
@Entity
public class PlantProtectionProduct implements EntityId {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(nullable = false)
    private String authorizationNumber;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String type;
    @Column(nullable = false,length = 1000)
    private String registeredOffice;
    @Column(nullable = false,length = 1000)
    private String manufacturer;
    @Column(nullable = false,length = 1000)
    private String activeSubstance;
    @Column(length = 1000)
    private String classificationHumanHealth;
    @Column(length = 1000)
    private String classificationBeesHazard;
    @Column(length = 1000)
    private String classificationAquaticOrganisms;
    @Column(length = 1000)
    private String classificationEnvironmentalHazards;
    @Column(nullable = false)
    private LocalDate validityTerm;
    @Column(nullable = false)
    private LocalDate salePeriod;
    private LocalDate usePeriod;
    @Column(length = 1500)
    private String packagingType;


    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id=id;
    }

    /**
     * Gets authorization number.
     *
     * @return the authorization number
     */
    public String getAuthorizationNumber() {
        return authorizationNumber;
    }

    /**
     * Sets authorization number.
     *
     * @param authorizationNumber the authorization number
     */
    public void setAuthorizationNumber(String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets registered office.
     *
     * @return the registered office
     */
    public String getRegisteredOffice() {
        return registeredOffice;
    }

    /**
     * Sets registered office.
     *
     * @param registeredOffice the registered office
     */
    public void setRegisteredOffice(String registeredOffice) {
        this.registeredOffice = registeredOffice;
    }

    /**
     * Gets manufacturer.
     *
     * @return the manufacturer
     */
    public String getManufacturer() {
        return manufacturer;
    }

    /**
     * Sets manufacturer.
     *
     * @param manufacturer the manufacturer
     */
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    /**
     * Gets active substance.
     *
     * @return the active substance
     */
    public String getActiveSubstance() {
        return activeSubstance;
    }

    /**
     * Sets active substance.
     *
     * @param activeSubstance the active substance
     */
    public void setActiveSubstance(String activeSubstance) {
        this.activeSubstance = activeSubstance;
    }

    /**
     * Gets classification human health.
     *
     * @return the classification human health
     */
    public String getClassificationHumanHealth() {
        return classificationHumanHealth;
    }

    /**
     * Sets classification human health.
     *
     * @param classificationHumanHealth the classification human health
     */
    public void setClassificationHumanHealth(String classificationHumanHealth) {
        this.classificationHumanHealth = classificationHumanHealth;
    }

    /**
     * Gets classification bees hazard.
     *
     * @return the classification bees hazard
     */
    public String getClassificationBeesHazard() {
        return classificationBeesHazard;
    }

    /**
     * Sets classification bees hazard.
     *
     * @param classificationBeesHazard the classification bees hazard
     */
    public void setClassificationBeesHazard(String classificationBeesHazard) {
        this.classificationBeesHazard = classificationBeesHazard;
    }

    /**
     * Gets classification aquatic organisms.
     *
     * @return the classification aquatic organisms
     */
    public String getClassificationAquaticOrganisms() {
        return classificationAquaticOrganisms;
    }

    /**
     * Sets classification aquatic organisms.
     *
     * @param classificationAquaticOrganisms the classification aquatic organisms
     */
    public void setClassificationAquaticOrganisms(String classificationAquaticOrganisms) {
        this.classificationAquaticOrganisms = classificationAquaticOrganisms;
    }

    /**
     * Gets classification environmental hazards.
     *
     * @return the classification environmental hazards
     */
    public String getClassificationEnvironmentalHazards() {
        return classificationEnvironmentalHazards;
    }

    /**
     * Sets classification environmental hazards.
     *
     * @param classificationEnvironmentalHazards the classification environmental hazards
     */
    public void setClassificationEnvironmentalHazards(String classificationEnvironmentalHazards) {
        this.classificationEnvironmentalHazards = classificationEnvironmentalHazards;
    }

    /**
     * Gets validity term.
     *
     * @return the validity term
     */
    public LocalDate getValidityTerm() {
        return validityTerm;
    }

    /**
     * Sets validity term.
     *
     * @param validityTerm the validity term
     */
    public void setValidityTerm(LocalDate validityTerm) {
        this.validityTerm = validityTerm;
    }

    /**
     * Gets sale period.
     *
     * @return the sale period
     */
    public LocalDate getSalePeriod() {
        return salePeriod;
    }

    /**
     * Sets sale period.
     *
     * @param salePeriod the sale period
     */
    public void setSalePeriod(LocalDate salePeriod) {
        this.salePeriod = salePeriod;
    }

    /**
     * Gets use period.
     *
     * @return the use period
     */
    public LocalDate getUsePeriod() {
        return usePeriod;
    }

    /**
     * Sets use period.
     *
     * @param usePeriod the use period
     */
    public void setUsePeriod(LocalDate usePeriod) {
        this.usePeriod = usePeriod;
    }

    /**
     * Gets packaging type.
     *
     * @return the packaging type
     */
    public String getPackagingType() {
        return packagingType;
    }

    /**
     * Sets packaging type.
     *
     * @param packagingType the packaging type
     */
    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    @Override
    public String toString() {
        return "PlantProtectionProduct{" +
                "id=" + id +
                ", authorizationNumber='" + authorizationNumber + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", registeredOffice='" + registeredOffice + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", activeSubstance='" + activeSubstance + '\'' +
                ", classificationHumanHealth='" + classificationHumanHealth + '\'' +
                ", classificationBeesHazard='" + classificationBeesHazard + '\'' +
                ", classificationAquaticOrganisms='" + classificationAquaticOrganisms + '\'' +
                ", classificationEnvironmentalHazards='" + classificationEnvironmentalHazards + '\'' +
                ", validityTerm=" + validityTerm +
                ", salePeriod=" + salePeriod +
                ", usePeriod=" + usePeriod +
                ", packagingType='" + packagingType + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlantProtectionProduct product = (PlantProtectionProduct) o;
        return Objects.equals(id, product.id) &&
                Objects.equals(authorizationNumber, product.authorizationNumber) &&
                Objects.equals(name, product.name) &&
                Objects.equals(type, product.type) &&
                Objects.equals(registeredOffice, product.registeredOffice) &&
                Objects.equals(manufacturer, product.manufacturer) &&
                Objects.equals(activeSubstance, product.activeSubstance) &&
                Objects.equals(classificationHumanHealth, product.classificationHumanHealth) &&
                Objects.equals(classificationBeesHazard, product.classificationBeesHazard) &&
                Objects.equals(classificationAquaticOrganisms, product.classificationAquaticOrganisms) &&
                Objects.equals(classificationEnvironmentalHazards, product.classificationEnvironmentalHazards) &&
                Objects.equals(validityTerm, product.validityTerm) &&
                Objects.equals(salePeriod, product.salePeriod) &&
                Objects.equals(usePeriod, product.usePeriod) &&
                Objects.equals(packagingType, product.packagingType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, authorizationNumber, name, type, registeredOffice, manufacturer, activeSubstance, classificationHumanHealth, classificationBeesHazard, classificationAquaticOrganisms, classificationEnvironmentalHazards, validityTerm, salePeriod, usePeriod, packagingType);
    }
}
