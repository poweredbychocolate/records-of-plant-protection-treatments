package ewidencja.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * The type Agro technical treatments record.
 * @author Dawid
 * @version 1
 * @since 2019-05-08
 */
@Entity
public class AgroTechnicalTreatmentsRecord implements Serializable,EntityId {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(nullable = false)
    private LocalDate date;
    // species of agricultural crops
    @Column(nullable = false)
    private String species;
    // total crop area
    @Column(nullable = false)
    private Double totalArea;
    // area of the field where the treatment was performed
    @Column(nullable = false)
    private Double treatmentArea;
    // fertilizer type or name
    @Column(nullable = false)
    private String fertilizerType;
    // the dose of the fertilizer kg/10 000 m^2
    @Column(nullable = false)
    private Double fertilizerDose;
    // the dose of the fertilizer kg/total area
    @Column(nullable = false)
    private Double fertilizerDoseOnArea;

    /**
     * Instantiates a new Agro technical treatments record.
     */
    public AgroTechnicalTreatmentsRecord(){
        id=0;
    }


    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id=id;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * Gets species.
     *
     * @return the species
     */
    public String getSpecies() {
        return species;
    }

    /**
     * Sets species.
     *
     * @param species the species
     */
    public void setSpecies(String species) {
        this.species = species;
    }

    /**
     * Gets total area.
     *
     * @return the total area
     */
    public Double getTotalArea() {
        return totalArea;
    }

    /**
     * Sets total area.
     *
     * @param totalArea the total area
     */
    public void setTotalArea(Double totalArea) {
        this.totalArea = totalArea;
    }

    /**
     * Gets treatment area.
     *
     * @return the treatment area
     */
    public Double getTreatmentArea() {
        return treatmentArea;
    }

    /**
     * Sets treatment area.
     *
     * @param treatmentArea the treatment area
     */
    public void setTreatmentArea(Double treatmentArea) {
        this.treatmentArea = treatmentArea;
    }

    /**
     * Gets fertilizer type.
     *
     * @return the fertilizer type
     */
    public String getFertilizerType() {
        return fertilizerType;
    }

    /**
     * Sets fertilizer type.
     *
     * @param fertilizerType the fertilizer type
     */
    public void setFertilizerType(String fertilizerType) {
        this.fertilizerType = fertilizerType;
    }

    /**
     * Gets fertilizer dose.
     *
     * @return the fertilizer dose
     */
    public Double getFertilizerDose() {
        return fertilizerDose;
    }

    /**
     * Sets fertilizer dose.
     *
     * @param fertilizerDose the fertilizer dose
     */
    public void setFertilizerDose(Double fertilizerDose) {
        this.fertilizerDose = fertilizerDose;
    }

    /**
     * Gets fertilizer dose on area.
     *
     * @return the fertilizer dose on area
     */
    public Double getFertilizerDoseOnArea() {
        return fertilizerDoseOnArea;
    }

    /**
     * Sets fertilizer dose on area.
     *
     * @param fertilizerDoseOnArea the fertilizer dose on area
     */
    public void setFertilizerDoseOnArea(Double fertilizerDoseOnArea) {
        this.fertilizerDoseOnArea = fertilizerDoseOnArea;
    }
}
