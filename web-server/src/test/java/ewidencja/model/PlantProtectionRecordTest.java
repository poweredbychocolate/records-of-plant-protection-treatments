package ewidencja.model;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;


class PlantProtectionRecordTest {

    private final Integer id = 123;
    private final LocalDate date = LocalDate.now();
    private final String plant = "Rose of Citadel";
    private final Double totalArea = 100.0;
    private final Double treatmentArea = 78.3;
    private final String areaName = "Eden Prime";
    private final String protectionName = "Asari Touch";
    private final Double protectionDose = 1.23;
    private final Double liquidDose = 105.3;
    private final String reasonUse = "Reapers is here";
    private final String comments = "Because reapers attack us we try protect our human using asari touch";

    @Test
    void entityTest() {
        User user = new User();
        user.setId(102);
        user.setType(User.TYPE_PLANT_PROTECTION);
        user.setFirstName("Ashley");
        user.setLastName("Williams");
        user.setObtainingDate(LocalDate.now().minusYears(1).minusDays(10));
        assertNotNull(user);

        PlantProtectionRecord record = new PlantProtectionRecord();
        record.setUser(user);
        assertNotNull(record);
        assertEquals(user, record.getUser());

        record.setId(id);
        record.setDate(date);
        record.setPlant(plant);
        record.setTotalArea(totalArea);
        record.setTreatmentArea(treatmentArea);
        record.setAreaName(areaName);
        record.setProtectionName(protectionName);
        record.setProtectionDose(protectionDose);
        record.setLiquidDose(liquidDose);
        record.setReasonUse(reasonUse);
        record.setComments(comments);

        assertNotNull(record.getId());
        assertNotNull(record.getDate());
        assertNotNull(record.getPlant());
        assertNotNull(record.getTotalArea());
        assertNotNull(record.getTreatmentArea());
        assertNotNull(record.getAreaName());
        assertNotNull(record.getProtectionName());
        assertNotNull(record.getProtectionDose());
        assertNotNull(record.getLiquidDose());
        assertNotNull(record.getReasonUse());
        assertNotNull(record.getComments());
        assertNotNull(record.toString());

        assertEquals(id, record.getId());
        assertEquals(date, record.getDate());
        assertEquals(plant, record.getPlant());
        assertEquals(totalArea, record.getTotalArea());
        assertEquals(treatmentArea, record.getTreatmentArea());
        assertEquals(areaName, record.getAreaName());
        assertEquals(protectionName, record.getProtectionName());
        assertEquals(protectionDose, record.getProtectionDose());
        assertEquals(liquidDose, record.getLiquidDose());
        assertEquals(reasonUse, record.getReasonUse());

    }

}