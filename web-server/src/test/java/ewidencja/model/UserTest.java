package ewidencja.model;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type User test.
 */
class UserTest {

    private final Integer id=3;
    private final String brokenType="Mouse Hunter";
    private final String firstName="Liara";
    private final String lastName="T'Soni";
    private final long validityYears=5;
    private final LocalDate obtainingDate=LocalDate.now().minusYears(validityYears);

    /**
     * User test.
     */
    @Test
    void userTest(){
        User user = new User();
        assertNotNull(user);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setId(id);

        assertFalse(user.setType(brokenType));
        assertTrue(user.setType(User.TYPE_PLANT_PROTECTION));
        assertEquals(User.TYPE_PLANT_PROTECTION,user.getType());

        user.setObtainingDate(LocalDate.now());
        assertTrue(Validator.hasValidCertificate(user));
        user.setObtainingDate(obtainingDate);
        assertEquals(obtainingDate,user.getObtainingDate());
        assertFalse(Validator.hasValidCertificate(user));
        user.setObtainingDate(user.getObtainingDate().plusDays(1));
        assertTrue(Validator.hasValidCertificate(user));

        assertEquals(id,user.getId());
        assertEquals(lastName,user.getLastName());
        assertEquals(firstName,user.getFirstName());
        assertTrue(user.toString().length()>0);
    }
}