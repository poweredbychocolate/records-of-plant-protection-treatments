package ewidencja.rest.controllers;

import ewidencja.model.AgriculturalSale;
import ewidencja.model.PlantProtectionProduct;
import ewidencja.services.DatabaseService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@WebMvcTest(PlantProtectionProductController.class)
class PlantProtectionProductControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private DatabaseService databaseService;
    private Map<Integer, PlantProtectionProduct> mockList = new HashMap<>();
    private int index = 0;
    private final String json3 = "{\"authorizationNumber\":\"R-44/2007\",\"name\":\"Accent 75 WG\",\"type\":\"Chwastobójczy\"," +
            "\"registeredOffice\":\"DuPont Poland Sp. z o.o. ul. Postępu, 02-676 Warszawa\"," +
            "\"manufacturer\":\"DuPont International Operations Sarl - Konfederacja Szwajcarska\"," +
            "\"activeSubstance\":\"nikosulfuron - 75 % \",\"classificationHumanHealth\":\", H400, H410\"," +
            "\"classificationBeesHazard\":\"nie klasyfikuje się ze względu na niskie ryzyko\"," +
            "\"classificationAquaticOrganisms\":\"niebezpieczny dla środowiska, N oraz zwrot R51/53\"," +
            "\"classificationEnvironmentalHazards\":\"\",\"validityTerm\":\"2017-12-03\"," +
            "\"salePeriod\":\"2018-06-03\",\"usePeriod\":\"2019-06-03\",\"packagingType\":\"opakowania 80-800g HDPE\"}";
    private final String json1 = "{\"authorizationNumber\":\"R-102/2016\",\"name\":\"Ambrossio 500 SC\",\"type\":\"Fungicyd\"," +
            "\"registeredOffice\":\"Innvigo Sp. z o.o. Al. Jerozolimskie, 02-486 Warszawa\"," +
            "\"manufacturer\":\"Innvigo Sp. z o.o. - Warszawa\",\"activeSubstance\":\"tebukonazol - 500 g\"," +
            "\"classificationHumanHealth\":\" H302, H361d, H410\",\"classificationBeesHazard\":\"\"," +
            "\"classificationAquaticOrganisms\":\"\",\"classificationEnvironmentalHazards\":\"\"," +
            "\"validityTerm\":\"2020-08-31\",\"salePeriod\":\"2021-02-28\",\"usePeriod\":\"2022-02-28\"," +
            "\"packagingType\":\"opakowania 0,5: 1; 3; 5; 10; 20l HDPE\"}";
    private final String json2 = "{\"authorizationNumber\":\"R-7/2018\",\"name\":\"Zakeo Opti\",\"type\":\"Fungicyd\"," +
            "\"registeredOffice\":\"Syngenta Polska Sp. z o.o. ul. Szamocka, 01-748 Warszawa\"," +
            "\"manufacturer\":\"Syngenta Supply AG - Konfederacja Szwajcarska\"," +
            "\"activeSubstance\":\"chlorotalonil - 400 g,azoksystrobina - 80 g\"," +
            "\"classificationHumanHealth\":\", H317, H318, H332, H335, H400, H410, H351\",\"classificationBeesHazard\":\"\"," +
            "\"classificationAquaticOrganisms\":\"\",\"classificationEnvironmentalHazards\":\"\"," +
            "\"validityTerm\":\"2020-10-31\",\"salePeriod\":\"2021-04-30\",\"usePeriod\":\"2022-04-30\"," +
            "\"packagingType\":\"opakowania 0,25; 0,5; 1l PET, HDPE, opakowania 5; 7,5; 10; 20l HDPE\"}";

    @Test
    void test() throws Exception {
        when(databaseService.save(any(PlantProtectionProduct.class))).thenAnswer(invocationOnMock -> {
            System.err.println("[Save Call]");
            PlantProtectionProduct product = invocationOnMock.getArgument(0);
            System.err.println("[" + product.toString() + "]");
            if (product.getId() != null && mockList.containsKey(product.getId())) {
                return Optional.empty();
            } else {
                product.setId(index++);
                mockList.put(product.getId(), product);
                return Optional.of(product);
            }
        });
        when(databaseService.findAll((PlantProtectionProduct.class))).thenAnswer(invocationOnMock -> {
            System.err.println("[Find All Call]");
            return Optional.of(mockList.values());
        });
        when(databaseService.findById(eq(PlantProtectionProduct.class), any(Integer.class))).thenAnswer(invocationOnMock -> {
            System.err.println("[Find By Id Call]");
            int i = invocationOnMock.getArgument(1);
            System.err.println("[Id : " + i + " ]");
            PlantProtectionProduct product = mockList.get(i);
            System.err.println("[ " + product + " ]");
            return Optional.ofNullable(product);
        });
        when(databaseService.findProductNames(anyString())).thenAnswer(invocationOnMock -> {
            System.err.println("[Find Product Names by patten Call]");
            List<String> l = mockList.values().stream()
                    .filter(plantProtectionProduct -> plantProtectionProduct.getName().startsWith(invocationOnMock.getArgument(0)))
                    .map(PlantProtectionProduct::getName).distinct().sorted().collect(Collectors.toList());
            System.err.println("[ size: " + l.size() + " ]");
            return Optional.of(l);
        });
        when(databaseService.findAllProduct()).thenAnswer(invocationOnMock -> {
            System.err.println("[Find All Product Call]");
            List<PlantProtectionProduct> l = mockList.values().stream().sorted(Comparator.comparing(PlantProtectionProduct::getName)).collect(Collectors.toList());
            System.err.println("[ size: " + l.size() + " ]");
            return Optional.of(l);
        });
        when(databaseService.findProductTypes()).thenAnswer(invocationOnMock -> {
            System.err.println("[Find Product Types Call]");
            List<String> l = mockList.values().stream()
                    .map(PlantProtectionProduct::getType).distinct().sorted().collect(Collectors.toList());
            System.err.println("[ size: " + l.size() + " ]");
            return Optional.of(l);
        });

        //save test
        mockMvc.perform(MockMvcRequestBuilders.post("/plant/protection/product")
                .content(json1)
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        mockMvc.perform(MockMvcRequestBuilders.post("/plant/protection/product")
                .content(json2)
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        mockMvc.perform(MockMvcRequestBuilders.post("/plant/protection/product")
                .content(json3)
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        //get list test
        mockMvc.perform(MockMvcRequestBuilders.get("/plant/protection/product"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[3]").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$[2]").exists());
        // find by id test
        mockMvc.perform(MockMvcRequestBuilders.get("/plant/protection/product/find/{id}", 1))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.authorizationNumber").value("R-7/2018"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Zakeo Opti"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.type").value("Fungicyd"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.validityTerm").value("2020-10-31"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salePeriod").value("2021-04-30"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.usePeriod").value("2022-04-30"));
        mockMvc.perform(MockMvcRequestBuilders.get("/agricultural/find/{id}", 20))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound());
        //get names by pattern test
        mockMvc.perform(MockMvcRequestBuilders.get("/plant/protection/product/names/"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[2]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").value("Accent 75 WG"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").value("Ambrossio 500 SC"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2]").value("Zakeo Opti"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[3]").doesNotExist());
        mockMvc.perform(MockMvcRequestBuilders.get("/plant/protection/product/names/{pattern}", "A"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").value("Accent 75 WG"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").value("Ambrossio 500 SC"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2]").doesNotExist());
        mockMvc.perform(MockMvcRequestBuilders.get("/plant/protection/product/sorted"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[2]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Accent 75 WG"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value("Ambrossio 500 SC"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].name").value("Zakeo Opti"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[3]").doesNotExist());
        // get product types
        mockMvc.perform(MockMvcRequestBuilders.get("/plant/protection/product/types"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").value("Chwastobójczy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").value("Fungicyd"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2]").doesNotExist());
    }

}