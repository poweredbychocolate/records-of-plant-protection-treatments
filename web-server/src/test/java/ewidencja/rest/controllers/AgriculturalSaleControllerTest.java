package ewidencja.rest.controllers;

import ewidencja.model.AgriculturalSale;
import ewidencja.services.DatabaseService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.time.Month;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.mockito.Mockito.*;

/**
 * The type Agricultural sale controller test.
 *
 * @author Dawid
 * @version 1
 * @since 2020-01-14
 */
@WebMvcTest(AgriculturalSaleController.class)
class AgriculturalSaleControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private DatabaseService databaseService;
    private Map<Integer, AgriculturalSale> mockList = new HashMap<>();
    private int index = 0;

    /**
     * Test.
     *
     * @throws Exception the exception
     */
    @Test
    void test() throws Exception {
        when(databaseService.save(any(AgriculturalSale.class))).thenAnswer(invocationOnMock -> {
            System.err.println("[Save Call]");
            AgriculturalSale sale = invocationOnMock.getArgument(0);
            System.err.println("[" + sale.toString() + "]");
            if (sale.getId() != null && mockList.containsKey(sale.getId())) {
                return Optional.empty();
            } else {
                sale.setId(index++);
                mockList.put(sale.getId(), sale);
                return Optional.of(sale);
            }
        });
        when(databaseService.findAll((AgriculturalSale.class))).thenAnswer(invocationOnMock -> {
            System.err.println("[Find All Call]");
            return Optional.of(mockList.values());
        });
        when(databaseService.findById(eq(AgriculturalSale.class), any(Integer.class))).thenAnswer(invocationOnMock -> {
            System.err.println("[Find By Id Call]");
            int i = invocationOnMock.getArgument(1);
            System.err.println("[Id : " + i + " ]");
            AgriculturalSale sale = mockList.get(i);
            System.err.println("[ " + sale + " ]");
            return Optional.ofNullable(sale);
        });
        when(databaseService.findSaleNames()).thenAnswer(invocationOnMock -> {
            System.err.println("[Find Sale Name Call]");
            List<String> l = mockList.values().stream().map(AgriculturalSale::getName).distinct().sorted().collect(Collectors.toList());
            System.err.println("[ size: " + l.size() + " ]");
            return Optional.ofNullable(l);
        });
        when(databaseService.findSalesNames(anyString())).thenAnswer(invocationOnMock -> {
            System.err.println("[Find Sale Name by patten Call]");
            List<String> l = mockList.values().stream()
                    .filter(agriculturalSale -> agriculturalSale.getName().startsWith(invocationOnMock.getArgument(0)))
                    .map(AgriculturalSale::getName).distinct().sorted().collect(Collectors.toList());
            System.err.println("[ size: " + l.size() + " ]");
            return Optional.ofNullable(l);
        });
        when(databaseService.findSaleDates()).thenAnswer(invocationOnMock -> {
            System.err.println("[Find Sale date Call]");
            List<String> l = mockList.values().stream().map(a -> a.getDate().toString()).distinct().sorted().collect(Collectors.toList());
            System.err.println("[ size: " + l.size() + " ]");
            return Optional.ofNullable(l);
        });
        when(databaseService.findAllSale(anyString(), anyString())).thenAnswer(invocationOnMock -> {
            String date = invocationOnMock.getArgument(0);
            String name = invocationOnMock.getArgument(1);
            System.err.println("[Find All Sale Call]");
            Predicate<AgriculturalSale> predicate = null;
            if (name != null) {
                predicate = agriculturalSale -> agriculturalSale.getName().startsWith(name);
            }
            if (date != null) {
                Predicate<AgriculturalSale> tmp = agriculturalSale -> agriculturalSale.getDate().isBefore(LocalDate.of(Integer.parseInt(date), Month.DECEMBER, 31));
                tmp = tmp.and(agriculturalSale -> agriculturalSale.getDate().isAfter(LocalDate.of(Integer.parseInt(date), Month.JANUARY, 1)));
                if (predicate == null) {
                    predicate = tmp;
                } else {
                    predicate = predicate.and(tmp);
                }
            }
            List<AgriculturalSale> l = null;
            if (predicate != null) {
                l = mockList.values().stream().filter(predicate).sorted().collect(Collectors.toList());
                System.err.println("[ size: " + l.size() + " ]");
            }
            return Optional.of(l);
        });
        when(databaseService.remove(eq(AgriculturalSale.class), any(AgriculturalSale.class))).thenAnswer(invocationOnMock -> {
            AgriculturalSale agriculturalSale = invocationOnMock.getArgument(1);
            if (mockList.containsKey(agriculturalSale.getId())) {
                AgriculturalSale tmp = mockList.get(agriculturalSale.getId());
                if (tmp.equals(agriculturalSale)) {
                    mockList.remove(agriculturalSale.getId());
                    return true;
                }
            }
            return false;
        });
        String jsonSale = "{\"date\": \"2019-08-24\",\"name\": \"Potato\",\"price\": 10.23,\"weight\": 125.0}";
        System.err.println(jsonSale);
        //save test
        mockMvc.perform(MockMvcRequestBuilders.post("/agricultural")
                .content(jsonSale)
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("0"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Potato"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price").value("10.23"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.weight").value("125.0"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.date").value("2019-08-24"));
        jsonSale = "{\"date\": \"2019-08-24\",\"name\": \"Cucumber\",\"price\": 10.23,\"weight\": 125.0}";
        mockMvc.perform(MockMvcRequestBuilders.post("/agricultural")
                .content(jsonSale)
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Cucumber"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price").value("10.23"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.weight").value("125.0"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.date").value("2019-08-24"));
        jsonSale = "{\"date\": \"2019-08-24\",\"id\" : 1,\"name\": \"Cucumber\",\"price\": 10.23,\"weight\": 125.0}";
        mockMvc.perform(MockMvcRequestBuilders.post("/agricultural")
                .content(jsonSale)
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isConflict());
        //get list test
        mockMvc.perform(MockMvcRequestBuilders.get("/agricultural"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(0))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2]").doesNotExist());
        // get by id test
        mockMvc.perform(MockMvcRequestBuilders.get("/agricultural/find/{id}", 1))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Cucumber"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price").value("10.23"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.weight").value("125.0"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.date").value("2019-08-24"));
        mockMvc.perform(MockMvcRequestBuilders.get("/agricultural/find/{id}", 2))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound());
        //get names test
        mockMvc.perform(MockMvcRequestBuilders.get("/agricultural/names"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").value("Cucumber"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").value("Potato"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2]").doesNotExist());
        //get names by pattern test
        mockMvc.perform(MockMvcRequestBuilders.get("/agricultural/names/{pattern}", "Pot"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").value("Potato"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").doesNotExist());
        mockMvc.perform(MockMvcRequestBuilders.get("/agricultural/names/{pattern}", "Ras"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound()).andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
        //get dates test
        mockMvc.perform(MockMvcRequestBuilders.get("/agricultural/dates"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").value("2019-08-24"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").doesNotExist());
        //get filter test
        mockMvc.perform(MockMvcRequestBuilders.get("/agricultural/filter/{date}/{name}", 2019, "Cucu"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Cucumber"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").doesNotExist());
        mockMvc.perform(MockMvcRequestBuilders.get("/agricultural/filter/{date}/{name}", 2018, "Cucu"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").doesNotExist());
        //delete remove test
        mockMvc.perform(MockMvcRequestBuilders.delete("/agricultural/{id}", 0))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
        mockMvc.perform(MockMvcRequestBuilders.delete("/agricultural/{id}", 0))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
        mockMvc.perform(MockMvcRequestBuilders.delete("/agricultural/{id}", 2))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
        //post remove test
        mockMvc.perform(MockMvcRequestBuilders.post("/agricultural/remove")
                .content(jsonSale)
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Cucumber"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));

    }
}