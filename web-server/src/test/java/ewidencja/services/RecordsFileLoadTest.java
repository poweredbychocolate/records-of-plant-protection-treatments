package ewidencja.services;

import ewidencja.model.PlantProtectionProduct;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class RecordsFileLoadTest {
    @Autowired
    private RecordFileService service;
    @Autowired
    private DatabaseService databaseService;

    @Test
    void readPlantProtectionProducts() {
        assertNotNull(service);
        assertNotNull(databaseService);
        List<PlantProtectionProduct> localList = null;
        //try read not existing file
        try {
            localList = service.readPlantProtectionProducts(new FileInputStream(new File("test")));
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
        }
        assertNull(localList);
        //try read file from resources
        try {
            //TODO edit if sor file was edited
            localList = service.readPlantProtectionProducts(new FileInputStream(new File("src/test/resources/sor.xls")));
            assertNotNull(localList);
            assertTrue(databaseService.save(localList));
            assertEquals(2264, localList.size());
            assertEquals(27, databaseService.findProductTypes().get().size());
            assertEquals("Akarycyd", databaseService.findProductTypes().get().get(1));
            assertNotEquals("Akarycyd", databaseService.findProductTypes().get().get(5));
            String type = "Akarycyd";
            Optional<List<PlantProtectionProduct>> productList = databaseService.findAllProduct(null, type);
            assertTrue(productList.isPresent());
            assertTrue(productList.get().get(0).getType().contains(type));
            assertTrue(productList.get().get(2).getType().contains(type));
            assertTrue(productList.get().get(5).getType().contains(type));
            assertTrue(productList.get().get(10).getType().contains(type));
            assertTrue(productList.get().get(12).getType().contains(type));
            productList = databaseService.findAllProduct("as", null);
            assertTrue(productList.isPresent());
            assertTrue(productList.get().size() > 0);
            assertEquals(productList.get().size(), databaseService.findAllProduct("AS", null).get().size());
            assertEquals(productList.get().size(), databaseService.findAllProduct("As", null).get().size());
            assertEquals(productList.get().size(), databaseService.findAllProduct("aS", null).get().size());
            assertTrue(databaseService.findAllProduct("As", null).get().size() > databaseService.findAllProduct("As", "Fungicyd").get().size());
            System.err.println(databaseService.findProductNames("H").get());
            assertTrue(databaseService.findProductNames("A").isPresent());
            assertNotEquals(0,databaseService.findProductNames("A").get().size());
            assertTrue(databaseService.findProductNames("Abringo").isPresent());
            assertEquals(1,databaseService.findProductNames("Abringo").get().size());
            assertTrue(databaseService.findProductNames("He").isPresent());
            assertEquals(25,databaseService.findProductNames("He").get().size());
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
        }
    }
}