package ewidencja.services;

import ewidencja.model.PlantProtectionRecord;
import ewidencja.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Database service test.
 *
 * @author Dawid
 * @version 3
 */
@SpringBootTest
class DatabaseServiceTest {
    @Autowired
    private DatabaseService databaseService;
    private User user1, user2;
    private PlantProtectionRecord record1, record2, record3;


    @BeforeEach
    void BeforeEach() {
        user1 = new User();
        user1.setFirstName("Liara");
        user1.setLastName("T'Soni");
        user1.setType(User.TYPE_PLANT_PROTECTION);
        user1.setObtainingDate(LocalDate.now());

        user2 = new User();
        user2.setFirstName("Tali");
        user2.setLastName("Zorah");
        user2.setType(User.TYPE_PLANT_PROTECTION);
        user2.setObtainingDate(LocalDate.now().minusYears(3));

        record1 = new PlantProtectionRecord();
        record1.setDate(LocalDate.now().minusDays(5));
        record1.setPlant("Rose of Citadel");
        record1.setTotalArea(100.0);
        record1.setTreatmentArea(83.2);
        record1.setAreaName("Eden Prime");
        record1.setProtectionName("Asari Touch");
        record1.setProtectionDose(1.25);
        record1.setLiquidDose(20.0);
        record1.setReasonUse("Reapers is here");
        record1.setComments("Because reapers attack us we try protect our human using asari touch");

        record2 = new PlantProtectionRecord();
        record2.setDate(LocalDate.now().minusDays(3));
        record2.setPlant("Electric Bloody Drop");
        record2.setTotalArea(560.0);
        record2.setTreatmentArea(362.8);
        record2.setAreaName("Eden Prime");
        record2.setProtectionName("Quarians Star Dust");
        record2.setProtectionDose(2.56);
        record2.setLiquidDose(204.2);
        record2.setReasonUse("Black leaf");
        record2.setComments("Black leaf,same leaves on ground, rainy,windy");

        record3 = new PlantProtectionRecord();
        record3.setDate(LocalDate.now());
        record3.setPlant("Orange trees");
        record3.setTotalArea(60.0);
        record3.setTreatmentArea(60.0);
        record3.setAreaName("Thessia");
        record3.setProtectionName("Black drop");
        record3.setProtectionDose(4.56);
        record3.setLiquidDose(364.2);
        record3.setReasonUse("Dropping leaves");
        record3.setComments("Dropping leaves,sunny,windy");
    }

    /**
     * Test users.
     */
    @Test
    void testUsers() {

        assertNotNull(databaseService);
        Optional<User> lUser = databaseService.save(user1);
        assertNotNull(lUser);
        assertTrue(lUser.isPresent());
        assertNotNull(lUser.get());
        assertNotEquals(user1, lUser.get());
        assertEquals(user1.getFirstName(), lUser.get().getFirstName());
        assertEquals(user1.getLastName(), lUser.get().getLastName());
        Optional<User> tmpU = databaseService.save(lUser.get());
        assertTrue(tmpU.isPresent());
        assertEquals(lUser.get(), tmpU.get());
        tmpU = databaseService.save(user2);
        assertNotNull(tmpU);
        assertTrue(tmpU.isPresent());
        assertNotEquals(lUser.get(), tmpU.get());

        Optional<List<User>> list = databaseService.findAll(User.class);
        assertNotNull(list);
        assertTrue(list.isPresent());
        assertEquals(2, list.get().size());
        assertNotEquals(0, list.get().size());
        assertFalse(databaseService.remove(User.class, user2));
        for (User u : list.get()) {
            assertTrue(databaseService.remove(User.class, u));
        }
        assertEquals(0, databaseService.findAll(User.class).get().size());
        Optional<User> user10 = databaseService.save(user1);
        Optional<User> user20 = databaseService.save(user2);
        assertNotNull(user10);
        assertNotNull(user20);
        assertTrue(user10.isPresent());
        assertTrue(user20.isPresent());
        assertTrue(databaseService.remove(User.class, user20.get()));
        assertTrue(databaseService.remove(User.class, user10.get()));
        assertFalse(databaseService.remove(User.class, user2));
        assertFalse(databaseService.remove(User.class, user1));

        assertEquals(0, databaseService.findAll(User.class).get().size());
        user10 = databaseService.save(user1);
        user20 = databaseService.save(user2);
        assertTrue(user10.isPresent());
        assertTrue(user20.isPresent());
        assertNotEquals(0, databaseService.findAll(User.class).get().size());
        assertEquals(2, databaseService.findAll(User.class).get().size());
        assertNotNull(databaseService.findById(User.class, user10.get().getId()));
        assertNotNull(databaseService.findById(User.class, user20.get().getId()));
        assertEquals(user10, databaseService.findById(User.class, user10.get().getId()));
        assertEquals(user20, databaseService.findById(User.class, user20.get().getId()));
        assertNotEquals(user10, databaseService.findById(User.class, user20.get().getId()));
        assertNotEquals(user20, databaseService.findById(User.class, user10.get().getId()));
        assertFalse(databaseService.findById(User.class, 1000).isPresent());
        assertTrue(databaseService.remove(User.class, user20.get()));
        assertTrue(databaseService.remove(User.class, user10.get()));
    }

    @Test
    void testPlantProtectionRecord() {

        assertNotNull(databaseService);
        Optional<User> localUser1 = databaseService.save(user1);
        Optional<User> localUser2 = databaseService.save(user2);
        assertTrue(localUser1.isPresent());
        assertTrue(localUser2.isPresent());
        record1.setUser(localUser1.get());
        Optional<PlantProtectionRecord> localRecord1 = databaseService.save(record1);
        assertNotNull(localRecord1);
        assertTrue(localRecord1.isPresent());
        assertNotEquals(record1, localRecord1.get());
        assertEquals(record1.getDate(), localRecord1.get().getDate());
        assertEquals(record1.getPlant(), localRecord1.get().getPlant());
        assertEquals(record1.getTotalArea(), localRecord1.get().getTotalArea());
        assertEquals(record1.getTreatmentArea(), localRecord1.get().getTreatmentArea());
        assertEquals(record1.getAreaName(), localRecord1.get().getAreaName());
        assertEquals(record1.getProtectionName(), localRecord1.get().getProtectionName());
        assertEquals(record1.getProtectionDose(), localRecord1.get().getProtectionDose());
        assertEquals(record1.getLiquidDose(), localRecord1.get().getLiquidDose());
        assertEquals(record1.getReasonUse(), localRecord1.get().getReasonUse());
        assertEquals(record1.getComments(), localRecord1.get().getComments());
        assertEquals(record1.getUser(), localRecord1.get().getUser());
        assertNotEquals(record1, record2);
        assertNotEquals(record1, record3);
        assertNotEquals(record2, record3);

        record2.setUser(localUser1.get());
        record3.setUser(localUser2.get());
        Optional<PlantProtectionRecord> localRecord2 = databaseService.save(record2);
        Optional<PlantProtectionRecord> localRecord3 = databaseService.save(record3);
        assertNotNull(localRecord2);
        assertNotNull(localRecord3);
        assertNotEquals(localRecord2, localRecord3);
        assertEquals(3, databaseService.findAll(PlantProtectionRecord.class).get().size());
        ////
        Optional<List<PlantProtectionRecord>> list= databaseService.findAll(PlantProtectionRecord.class);
        assertTrue(list.isPresent());
        assertNotEquals(0, list.get().size());
        assertFalse(databaseService.remove(PlantProtectionRecord.class, record2));
        assertEquals(3, databaseService.findAll(PlantProtectionRecord.class).get().size());
        for (PlantProtectionRecord o : list.get()) {
            assertTrue(databaseService.remove(PlantProtectionRecord.class, o));
        }
        assertEquals(0, databaseService.findAll(PlantProtectionRecord.class).get().size());
        assertNotNull(localRecord1 = databaseService.save(record1));
        assertNotNull(localRecord2 = databaseService.save(record2));
        assertTrue(databaseService.remove(PlantProtectionRecord.class, localRecord1.get()));
        assertTrue(databaseService.remove(PlantProtectionRecord.class, localRecord2.get()));
        assertFalse(databaseService.remove(PlantProtectionRecord.class, record1));
        assertFalse(databaseService.remove(PlantProtectionRecord.class, record2));
        ////
        assertEquals(0, databaseService.findAll(PlantProtectionRecord.class).get().size());
        localRecord1 = databaseService.save(record1);
        localRecord2 = databaseService.save(record2);
        assertTrue(localRecord1.isPresent());
        assertTrue(localRecord2.isPresent());
        assertNotEquals(0, databaseService.findAll(PlantProtectionRecord.class).get().size());
        assertEquals(2, databaseService.findAll(PlantProtectionRecord.class).get().size());
        assertNotNull(databaseService.findById(PlantProtectionRecord.class, localRecord1.get().getId()));
        assertNotNull(databaseService.findById(PlantProtectionRecord.class, localRecord2.get().getId()));
        assertEquals(localRecord1, databaseService.findById(PlantProtectionRecord.class, localRecord1.get().getId()));
        assertEquals(localRecord2, databaseService.findById(PlantProtectionRecord.class, localRecord2.get().getId()));
        assertNotEquals(localRecord1, databaseService.findById(PlantProtectionRecord.class, localRecord2.get().getId()));
        assertNotEquals(localRecord2, databaseService.findById(PlantProtectionRecord.class, localRecord1.get().getId()));
        assertFalse(databaseService.findById(PlantProtectionRecord.class, 1000).isPresent());
    }
//
    @Test
    void findExpiringCertificates() {
        assertTrue(databaseService.save(user1).isPresent());
        assertTrue(databaseService.save(user2).isPresent());
        User u3 = new User();
        u3.setFirstName("Miranda");
        u3.setLastName("Lawson");
        u3.setType(User.TYPE_PLANT_PROTECTION);
        u3.setObtainingDate(LocalDate.now());
        User u4 = new User();
        u4.setFirstName("Test1");
        u4.setLastName("Test1");
        u4.setType(User.TYPE_PLANT_PROTECTION);
        u4.setObtainingDate(LocalDate.now().minusDays(1));
        User u5 = new User();
        u5.setFirstName("Test2");
        u5.setLastName("Test2");
        u5.setType(User.TYPE_PLANT_PROTECTION);
        u5.setObtainingDate(LocalDate.now().minusDays(2));
        User u6 = new User();
        u6.setFirstName("Test3");
        u6.setLastName("Test3");
        u6.setType(User.TYPE_PLANT_PROTECTION);
        u6.setObtainingDate(LocalDate.now().minusDays(3));
        User u7 = new User();
        u7.setFirstName("Test4");
        u7.setLastName("Test4");
        u7.setType(User.TYPE_PLANT_PROTECTION);
        u7.setObtainingDate(LocalDate.now().minusDays(4));
        User u8 = new User();
        u8.setFirstName("Test5");
        u8.setLastName("Test5");
        u8.setType(User.TYPE_PLANT_PROTECTION);
        u8.setObtainingDate(LocalDate.now().minusDays(5));

        assertTrue(databaseService.save(u3).isPresent());
        assertTrue(databaseService.save(u4).isPresent());
        assertTrue(databaseService.save(u5).isPresent());
        assertTrue(databaseService.save(u6).isPresent());
        assertTrue(databaseService.save(u7).isPresent());
        assertTrue(databaseService.save(u8).isPresent());
        Optional<List<User>> list =databaseService.findAll(User.class);
        assertTrue(list.isPresent());
        assertEquals(8, list.get().size());
        list = databaseService.findExpiringCertificates(LocalDate.now().minusDays(3));
        assertTrue(list.isPresent());
        assertNotNull(list.get());
        assertEquals(4, list.get().size());
    }
}